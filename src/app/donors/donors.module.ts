import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DonorsRoutingModule} from './donors-routing.module';
import {DonorsManagementPageComponent} from './components/donors-management-page/donors-management-page.component';
import {DonorsImportPageComponent} from './components/donors-import-page/donors-import-page.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [
    DonorsManagementPageComponent,
    DonorsImportPageComponent
  ],
  imports: [
    CommonModule,
    DonorsRoutingModule,
    SharedModule
  ]
})
export class DonorsModule {
}
