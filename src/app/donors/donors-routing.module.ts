import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DonorsManagementPageComponent} from './components/donors-management-page/donors-management-page.component';
import {DonorsImportPageComponent} from './components/donors-import-page/donors-import-page.component';

const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'RVB_DONORS'
    },
    children: [
      {
        path: '',
        redirectTo: 'management',
        pathMatch: 'full'
      },
      {
        path: 'management',
        component: DonorsManagementPageComponent,
        data: {
          breadcrumb: 'RVB_DONORS_MANAGEMENT'
        }
      },
      {
        path: 'import',
        component: DonorsImportPageComponent,
        data: {
          breadcrumb: null
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DonorsRoutingModule {
}
