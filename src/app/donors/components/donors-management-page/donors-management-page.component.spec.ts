import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DonorsManagementPageComponent } from './donors-management-page.component';

describe('DonorsManagementPageComponent', () => {
  let component: DonorsManagementPageComponent;
  let fixture: ComponentFixture<DonorsManagementPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonorsManagementPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonorsManagementPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
