import {Component} from '@angular/core';

@Component({
  selector: 'rvb-donors-management-page',
  templateUrl: './donors-management-page.component.html',
  styleUrls: ['./donors-management-page.component.scss']
})
export class DonorsManagementPageComponent {

  patients = [
    {
      name: 'Sergio',
      surname: 'Fernández',
      birthDate: '11/08/1645',
      dni: '45264815M',
      patientId: '753646',
      id: 0
    },
    {
      name: 'Marta',
      surname: 'Palau',
      birthDate: '02/08/2012',
      dni: '49466512H',
      patientId: '753799',
      id: 1
    },
    {
      name: 'Antonio',
      surname: 'Mota',
      birthDate: '07/08/1991',
      dni: '45264815M',
      patientId: '753162',
      id: 2
    },
    {
      name: 'Eloy',
      surname: 'Galdón',
      birthDate: '09/08/2000',
      dni: '49466512H',
      id: 3
    },
    {
      name: 'Sergio',
      surname: 'Fernández',
      birthDate: '11/08/1645',
      dni: '45264815M',
      id: 4
    },
    {
      name: 'Marta',
      surname: 'Palau',
      birthDate: '02/08/2012',
      dni: '49466512H',
      id: 5
    },
    {
      name: 'Antonio',
      surname: 'Mota',
      birthDate: '07/08/1991',
      dni: '45264815M',
      id: 6
    }
  ];

  constructor() {
  }

}
