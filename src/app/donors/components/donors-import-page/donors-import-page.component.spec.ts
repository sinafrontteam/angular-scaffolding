import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DonorsImportPageComponent } from './donors-import-page.component';

describe('DonorsImportPageComponent', () => {
  let component: DonorsImportPageComponent;
  let fixture: ComponentFixture<DonorsImportPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonorsImportPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonorsImportPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
