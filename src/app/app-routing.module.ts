import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: 'configuration', loadChildren: './configuration/configuration.module#ConfigurationModule'},
  {path: 'donors', loadChildren: './donors/donors.module#DonorsModule'},
  {path: 'core', redirectTo: 'core'},
  {path: '', redirectTo: 'donors', pathMatch: 'full'},
  {path: '**', redirectTo: 'donors', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
