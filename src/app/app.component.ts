import {Component} from '@angular/core';
import {I18nService} from './core/services/i18n.service';
import {ValidationService} from '@modules/core/services/validation.service';
import {validationRules} from '@constants/validation-rules.constants';

@Component({
  selector: 'rvb-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-scaffolding';
  validationRules = validationRules;

  constructor(i18nService: I18nService,
              validationService: ValidationService) {
    i18nService.setLang();
    validationService.setRules(this.validationRules);
  }
}
