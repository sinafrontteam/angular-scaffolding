import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ConfigurationRoutingModule} from './configuration-routing.module';
import {SharedModule} from '../shared/shared.module';
import {ConfTrainingTeamListPageComponent} from './components/conf-training-team/conf-training-team-list-page/conf-training-team-list-page.component';
import { ConfTrainingTeamCrudPageComponent } from './components/conf-training-team/conf-training-team-crud-page/conf-training-team-crud-page.component';

@NgModule({
  declarations: [
    ConfTrainingTeamListPageComponent,
    ConfTrainingTeamCrudPageComponent
  ],
  imports: [
    CommonModule,
    ConfigurationRoutingModule,
    SharedModule
  ]
})
export class ConfigurationModule {
}
