import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ConfTrainingTeam} from '@modules/configuration/models/conf-training-team.model';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  constructor(private http: HttpClient) {
  }

  createTrainingTeam(trainingTeam: ConfTrainingTeam): Promise<any> {
    const url = `/biobancos/equipocaptacion`;
    return this.http.post(url, trainingTeam).toPromise();
  }

  getTrainingTeam(trainingTeamId: number): Promise<ConfTrainingTeam> {
    const url = `/biobancos/equipocaptacion/${trainingTeamId}`;
    return this.http.get(url).toPromise();
  }

  updateTrainingTeam(trainingTeam: ConfTrainingTeam): Promise<any> {
    const url = `/biobancos/equipocaptacion`;
    return this.http.put(url, trainingTeam).toPromise();
  }
}
