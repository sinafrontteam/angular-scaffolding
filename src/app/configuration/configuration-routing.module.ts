import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ConfTrainingTeamListPageComponent} from './components/conf-training-team/conf-training-team-list-page/conf-training-team-list-page.component';
import {ConfTrainingTeamCrudPageComponent} from '@modules/configuration/components/conf-training-team/conf-training-team-crud-page/conf-training-team-crud-page.component';

const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'RVB_CONFIGURATION'
    },
    children: [
      {
        path: '',
        redirectTo: 'training-team',
        pathMatch: 'full'
      },
      {
        path: 'training-team',
        data: {
          breadcrumb: 'RVB_TRAINING_TEAM'
        },
        children: [
          {
            path: '',
            redirectTo: 'list',
            pathMatch: 'full'
          },
          {
            path: 'list',
            component: ConfTrainingTeamListPageComponent,
            data: {
              breadcrumb: null
            }
          },
          {
            path: 'create',
            component: ConfTrainingTeamCrudPageComponent,
            data: {
              breadcrumb: 'RVB_NEW'
            }
          },
          {
            path: ':trainingTeamId/update',
            component: ConfTrainingTeamCrudPageComponent,
            data: {
              breadcrumb: 'RVB_EDIT'
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationRoutingModule {
}
