
export class ConfTrainingTeamListItem {
  id: number;
  codigo?: string;
  nombre?: string;
}

export class ConfTrainingTeam {
  id?: number;
  codigo?: string;
  nombre?: string;
}
