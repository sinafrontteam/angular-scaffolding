import {Component, OnInit} from '@angular/core';
import {ConfTrainingTeam} from '@modules/configuration/models/conf-training-team.model';
import {ConfigurationService} from '@modules/configuration/services/configuration.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'rvb-conf-training-team-crud-page',
  templateUrl: './conf-training-team-crud-page.component.html',
  styleUrls: ['./conf-training-team-crud-page.component.scss']
})
export class ConfTrainingTeamCrudPageComponent implements OnInit {
  model: ConfTrainingTeam = <ConfTrainingTeam>{};
  trainingTeamId: number;

  constructor(private configurationService: ConfigurationService,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.trainingTeamId = parseInt(this.activatedRoute.snapshot.params['trainingTeamId'], 10) || null;
    this.getTrainingTeam();
  }

  async getTrainingTeam(): Promise<void> {
    if (!this.trainingTeamId) {
      return;
    }

    this.model = await this.configurationService.getTrainingTeam(this.trainingTeamId);
  }

  async onSubmit(): Promise<void> {
    if (this.trainingTeamId) {
      await this.updateTrainingTeam();
    } else {
      await this.createTrainingTeam();
    }
  }

  onCancel(): void {
    window.history.back();
  }

  async createTrainingTeam(): Promise<void> {
    await this.configurationService.createTrainingTeam(this.model);
    window.history.back();
  }

  async updateTrainingTeam(): Promise<void> {
    await this.configurationService.updateTrainingTeam(this.model);
    window.history.back();
  }

}
