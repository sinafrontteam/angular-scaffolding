import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTrainingTeamCrudPageComponent } from './conf-training-team-crud-page.component';

describe('ConfTrainingTeamNewPageComponent', () => {
  let component: ConfTrainingTeamCrudPageComponent;
  let fixture: ComponentFixture<ConfTrainingTeamCrudPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTrainingTeamCrudPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTrainingTeamCrudPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
