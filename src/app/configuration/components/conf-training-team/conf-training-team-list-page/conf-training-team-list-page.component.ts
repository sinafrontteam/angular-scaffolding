import {Component} from '@angular/core';

class ConfTrainingTeamFilter {
  codigo?: string;
  nombre?: string;
}

@Component({
  selector: 'rvb-conf-training-team-list-page',
  templateUrl: './conf-training-team-list-page.component.html',
  styleUrls: ['./conf-training-team-list-page.component.scss']
})
export class ConfTrainingTeamListPageComponent {
  filters: ConfTrainingTeamFilter = <ConfTrainingTeamFilter>{};
  defaultFilters = {...this.filters};
  resetTable = 1;

  constructor() {
  }

  applyFilters(): void {
    this.resetTableFunction();
  }

  resetTableFunction(): void {
    this.resetTable++;
  }

  cleanFilters(): void {
    this.filters = {...this.defaultFilters};
  }

  onDeleteItem(): void {
    this.resetTableFunction();
  }

}
