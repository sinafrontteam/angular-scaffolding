import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTrainingTeamListPageComponent } from './conf-training-team-list-page.component';

describe('ConfTrainingTeamPageComponent', () => {
  let component: ConfTrainingTeamListPageComponent;
  let fixture: ComponentFixture<ConfTrainingTeamListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTrainingTeamListPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTrainingTeamListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
