import {Component, Input} from '@angular/core';
import {Params} from '@angular/router';

@Component({
  selector: 'rvb-go-to-route-icon',
  templateUrl: './go-to-route-icon.component.html',
  styleUrls: ['./go-to-route-icon.component.scss']
})
export class GoToRouteIconComponent {
  @Input() tooltip: string;
  @Input() tooltipPosition = 'top';
  @Input() href: string;
  @Input() params: Params;
  @Input() icon: string;
  @Input() iconClass: string;
  @Input() showText: boolean;

  constructor() {
  }
}
