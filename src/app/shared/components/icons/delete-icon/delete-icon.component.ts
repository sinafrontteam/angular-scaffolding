import {Component, EventEmitter, Input, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'rvb-delete-icon',
  templateUrl: './delete-icon.component.html',
  styleUrls: ['./delete-icon.component.scss']
})
export class DeleteIconComponent {
  @Input() icon = 'icon-bin';
  @Input() tooltip: string;
  @Input() tooltipPosition = 'top';
  @Input() path: string;
  @Input() titleDialog: string;
  @Input() putMethod = false;
  @Output() updateChange = new EventEmitter<any>();
  displayDialog = false;

  constructor(private http: HttpClient) {
  }

  onClickIcon(event: Event): void {
    event.stopPropagation();
    this.displayDialog = true;
  }

  async onClickCloseDialog(action: boolean): Promise<any> {
    if (action) {
      await this.deleteItem();
      this.updateChange.emit();
    }

    this.displayDialog = false;
  }

  async deleteItem(): Promise<any> {
    if (!this.path) {
      return;
    }

    if (this.putMethod) {
      await this.http.put(this.path, {}).toPromise();
    } else {
      await this.http.delete(this.path).toPromise();
    }
  }

}
