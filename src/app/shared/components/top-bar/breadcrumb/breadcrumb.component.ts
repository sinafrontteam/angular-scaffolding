import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {filter} from 'rxjs/operators';
import {isNullOrUndefined} from 'util';
import {TranslateService} from '@ngx-translate/core';
import {BreadcrumbItem} from '@models/breadcrumb-item.model';

@Component({
  selector: 'rvb-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  breadcrumbs: BreadcrumbItem[];

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private translateService: TranslateService,
              private title: Title) {
  }

  ngOnInit(): void {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => this.onNavigationEnd());
  }

  onNavigationEnd(): void {
    this.breadcrumbs = this.createBreadcrumbs(this.activatedRoute.root);
    this.setBrowserTitle(this.breadcrumbs);
  }

  async setBrowserTitle(breadcrumbs: BreadcrumbItem[]): Promise<void> {
    let titleName = 'RVB';
    if (breadcrumbs && breadcrumbs.length) {
      const lastBreadcrumbName = breadcrumbs[breadcrumbs.length - 1].label;
      titleName = await this.translateService.get(lastBreadcrumbName).toPromise();
    }

    this.title.setTitle(titleName);
  }

  private createBreadcrumbs(route: ActivatedRoute, url: string = '', breadcrumbs: BreadcrumbItem[] = []): BreadcrumbItem[] {
    const children: ActivatedRoute[] = route.children;

    if (children.length === 0) {
      return breadcrumbs;
    }

    for (const child of children) {
      const routeURL: string = child.snapshot.url.map(segment => segment.path).join('/');
      if (routeURL !== '') {
        url += `/${routeURL}`;
      }

      const label = child.snapshot.data.breadcrumb;
      if (!isNullOrUndefined(label)) {
        breadcrumbs.push({label, url});
      }

      return this.createBreadcrumbs(child, url, breadcrumbs);
    }
  }
}
