import {Component} from '@angular/core';
import {DispatcherService} from '../../../core/services/dispatcher.service';

@Component({
  selector: 'rvb-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent {

  constructor(private dispatcher: DispatcherService) {
  }

  toggleNavSidebar(): void {
    this.dispatcher.exec('toggle-nav-sidebar');
  }

}
