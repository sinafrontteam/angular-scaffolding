import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {DispatcherService} from '@modules/core/services/dispatcher.service';
import {TranslateService} from '@ngx-translate/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'rvb-nav-sidebar',
  templateUrl: './nav-sidebar.component.html',
  styleUrls: ['./nav-sidebar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NavSidebarComponent implements OnInit {
  observerKey = 'nav-sidebar';
  collapsed = false;
  items: MenuItem[];

  constructor(private dispatcher: DispatcherService,
              private translateService: TranslateService) {
  }

  ngOnInit() {
    this.setItems();
    this.dispatcher.register('toggle-nav-sidebar', () => this.onToggleNavSidebar(), this.observerKey);
  }

  async setItems(): Promise<void> {
    const donorsText = await this.translateService.get('RVB_DONORS').toPromise();
    const donorsManagementText = this.translateService.instant('RVB_DONORS_MANAGEMENT');
    const donorsImportText = this.translateService.instant('RVB_DONORS_IMPORT');
    const configurationText = this.translateService.instant('RVB_CONFIGURATION');
    const trainingTeamText = this.translateService.instant('RVB_TRAINING_TEAM');

    this.items = [
      {
        label: donorsText,
        icon: 'icon-bloodbag',
        expanded: this.checkActiveState('/donors'),
        items: [
          {label: donorsManagementText, icon: '', routerLink: 'donors/management'},
          {label: donorsImportText, icon: '', routerLink: 'donors/import'}
        ]
      },
      {
        label: configurationText,
        icon: 'icon-settings',
        expanded: this.checkActiveState('/configuration'),
        items: [
          {label: trainingTeamText, routerLink: 'configuration/training-team'}
        ]
      },
      {
        label: 'Core components',
        icon: 'icon-dashboard',
        items: [
          {label: 'Core', icon: '', routerLink: 'core/doc/hola'}
        ]
      }
    ];
  }

  checkActiveState(givenLink): boolean {
    return window.location.pathname.includes(givenLink);
  }

  onToggleNavSidebar(): void {
    this.collapsed = !this.collapsed;
  }
}
