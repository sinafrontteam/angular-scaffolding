const validationRules = {
  'generic': {
    'fieldNotNull': {
      'notNull': {
        'message': 'RVB_RULES_ERROR_REQUIRED'
      }
    },
    'fieldNotNullLength': {
      'notNull': {
        'message': 'RVB_RULES_ERROR_REQUIRED'
      },
      'length': {
        'message': 'RVB_RULES_ERROR_LENGTH',
        'arguments': [0, 2000]
      }
    }
  }
};

export {
  validationRules
};
