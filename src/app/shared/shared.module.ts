import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TopBarComponent} from './components/top-bar/top-bar.component';
import {BreadcrumbComponent} from './components/top-bar/breadcrumb/breadcrumb.component';
import {CoreModule} from '../core/core.module';
import {NavSidebarComponent} from './components/nav-sidebar/nav-sidebar.component';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpAuthorizationInterceptor} from '@interceptors/http.interceptor';
import { ToastrModule } from 'ngx-toastr';
import {DeleteIconComponent} from '@shared/components/icons/delete-icon/delete-icon.component';
import {GoToRouteIconComponent} from '@shared/components/icons/go-to-route-icon/go-to-route-icon.component';

@NgModule({
  declarations: [
    TopBarComponent,
    BreadcrumbComponent,
    NavSidebarComponent,
    DeleteIconComponent,
    GoToRouteIconComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    CoreModule,
    TranslateModule,
    ToastrModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpAuthorizationInterceptor,
      multi: true
    }
  ],
  exports: [
    FormsModule,
    CoreModule,
    TranslateModule,
    TopBarComponent,
    BreadcrumbComponent,
    NavSidebarComponent,
    DeleteIconComponent,
    GoToRouteIconComponent
  ]
})
export class SharedModule {
}
