import {Component, Input, ViewEncapsulation} from '@angular/core';
import {ValidationRule} from '../../models/validation-rule.model';
import {NgModel} from '@angular/forms';
import {GenericFunctionsUtil} from '../../utils/generic-functions.util';

@Component({
  selector: 'si-validation-element',
  templateUrl: './validation-element.component.html',
  styleUrls: ['./validation-element.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ValidationElementComponent {
  @Input() formModel: NgModel;

  // Needed if the component is being used in AngularJS view and it needs validation.
  @Input() ng1Form: any;
  tooltipText: string;
  showRequiredElementFlag = false;

  constructor() {
  }

  showRequiredElement(): void {
    setTimeout(() => {
      this.showRequiredElementFlag = true;
    });
  }

  showWarning(invalidKey: string, validationRule: ValidationRule): void {
    if (invalidKey === 'notNull') {
      this.hideWarning();
      return;
    }

    this.tooltipText = GenericFunctionsUtil.stringReplaceArguments(validationRule.message, validationRule.arguments);
  }

  hideWarning(): void {
    this.tooltipText = null;
  }

}
