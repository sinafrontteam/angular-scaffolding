import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'si-list-dynamic',
  templateUrl: './list-dynamic.component.html',
  styleUrls: ['./list-dynamic.component.scss']
})
export class ListDynamicComponent {
  @Input() addButton = true;
  @Output() itemsChange = new EventEmitter<any[]>();
  itemAuxId = 0;
  private _items: any[];

  constructor() {
  }

  get items() {
    return this._items;
  }

  @Input()
  set items(val) {
    if (!val) {
      val = [];
    }

    if (val.length) {
      val.map(item => item.auxId = this.itemAuxId++);
    }

    this._items = val;
    this.itemsChange.emit(val);
  }

  onClickAddRow(): void {
    if (!this.items) {
      this.items = [];
    }

    this.items.unshift({
      auxId: this.itemAuxId++
    });

    this.itemsChange.emit(this.items);
  }

  onClickDeleteRow(index: number): void {
    this.items.splice(index, 1);
    this.itemsChange.emit(this.items);
  }
}
