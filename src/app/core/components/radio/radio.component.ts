import {Component, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {keyboardKeys} from '../../constants/keyboard-keys.constants';

@Component({
  selector: 'si-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss']
})
export class RadioComponent {
  @Input() text: string;
  @Input() class: string;
  @Input() id: string;
  @Input() name: string;
  @Input() value: any;
  @Input() model: any;
  @Output() modelChange = new EventEmitter<any>();
  @Input() disabled: boolean;
  private _checked;

  constructor() {
  }

  get checked() {
    return this._checked;
  }

  @Input()
  set checked(val) {
    this._checked = val;
    if ((val || val === '') && !this.model) {
      this.setValueToModel();
    }
  }

  setValueToModel() {
    this.model = this.value;
    this.onChangeModel(this.model);
  }

  onChangeModel(model) {
    this.modelChange.emit(model);
  }

  @HostListener('keydown', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key === keyboardKeys.ENTER) {
      this.setValueToModel();
    }
  }

}
