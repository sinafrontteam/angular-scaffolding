import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {OverlayPanel} from 'primeng/overlaypanel';

@Component({
  selector: 'si-overlay-panel',
  templateUrl: './overlay-panel.component.html',
  styleUrls: ['./overlay-panel.component.scss']
})
export class OverlayPanelComponent implements OnInit, OnDestroy {
  @ViewChild('pOverlayPanel') pOverlayPanel: OverlayPanel;
  onScrollBound;

  constructor() {
  }

  ngOnInit() {
    // We need to bind 'this' before setting this function as event listener.
    this.onScrollBound = this.onScroll.bind(this);
  }

  onScroll(): void {
    this.pOverlayPanel.hide();
    window.removeEventListener('scroll', this.onScrollBound, true);
  }

  toggle(event: MouseEvent): void {
    window.addEventListener('scroll', this.onScrollBound, true);
    this.pOverlayPanel.toggle(event);
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.onScrollBound, true);
  }
}
