import {Component, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {keyboardKeys} from '../../constants/keyboard-keys.constants';

@Component({
  selector: 'si-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent {
  @Input() text: string;
  @Input() placeholder: string;
  @Input() class: string;
  @Input() id: string;
  @Input() name: string;
  @Input() type = 'text';
  @Input() disabled: boolean;
  @Input() tooltipPosition: string;
  @Input() tooltipText: string;
  @Input() tooltipIcon: string;
  @Input() rule: string;
  @Input() min: number;
  @Input() max: number;
  @Input() model: any;
  @Output() modelChange = new EventEmitter<any>();
  @Output() keyEnterPress = new EventEmitter<any>();
  capsLock = false;
  lastKeyPressed: string;

  constructor() {
  }

  onChangeModel(model: any): void {
    if (this.lastKeyPressed === keyboardKeys.DOT && this.type === 'number') {
      return;
    }

    // Back webservices do not accept empty model.
    if (model === '') {
      model = undefined;
    }

    if (this.type === 'number') {
      if (model !== undefined && model !== null) {
        if (model % 1 === 0) {
          model = this.checkMinMaxModel(parseInt(model, 10));
        } else {
          model = this.checkMinMaxModel(parseFloat(model));
        }

      }

      this.model = model;
      this.modelChange.emit(model);
    } else {
      this.modelChange.emit(model);
    }
  }

  onClickArrow(addFlag: boolean): void {
    if (this.model || this.model === 0) {
      if (addFlag) {
        this.model += 1;
        this.onChangeModel(this.model);
      } else {
        this.model -= 1;
        this.onChangeModel(this.model);
      }
    } else {
      this.model = this.min || 0;
    }
  }

  checkMinMaxModel(number: number): number {
    if (!number && number !== 0) {
      return number;
    }

    if ((this.min || this.min === 0) && number < this.min) {
      return this.min;
    }

    if ((this.max || this.max === 0) && number > this.max) {
      return this.max;
    }

    return number;
  }

  @HostListener('keydown', ['$event'])
  async keyEvent(event: KeyboardEvent) {
    this.lastKeyPressed = event.key;

    if (this.type === 'password') {
      this.capsLock = event.getModifierState(keyboardKeys.CAPS_LOCK);
    }

    if (event.key === keyboardKeys.ENTER) {
      this.keyEnterPress.emit();
    }

    // If type is number, control if the user modifies the model using the arrow keys or it´s a negative number
    if (this.type === 'number' && (event.key === keyboardKeys.ARROW_DOWN || event.key === keyboardKeys.ARROW_UP || event.key === keyboardKeys.NUM_PAD_SUBTRACT)) {
      if (event.key === keyboardKeys.ARROW_DOWN && (this.min || this.min == 0) && this.model == this.min
        || event.key === keyboardKeys.ARROW_UP && (this.max || this.max == 0) && this.model == this.max
        || event.key === keyboardKeys.NUM_PAD_SUBTRACT && this.min >= 0) {
        event.preventDefault();
        return;
      }
    }
  }

  @HostListener('mousewheel', ['$event'])
  onMousewheel(event: any) {
    if (event.wheelDelta > 0 && this.type === 'number' && (this.max || this.max == 0) && this.model == this.max
      || event.wheelDelta < 0 && this.type === 'number' && (this.min || this.min == 0) && this.model == this.min) {
      event.preventDefault();
      return;
    }
  }
}
