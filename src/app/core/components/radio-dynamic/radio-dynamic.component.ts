import {Component, EventEmitter, Input, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IdName} from '../../models/id-name.model';

@Component({
  selector: 'si-radio-dynamic',
  templateUrl: './radio-dynamic.component.html',
  styleUrls: ['./radio-dynamic.component.scss']
})
export class RadioDynamicComponent {
  @Input() text: string;
  @Input() class: string;
  @Input() id: string;
  @Input() vertical = false;
  @Input() valueAttr: string;
  @Output() modelChange = new EventEmitter<any>();
  @Input() disabled: boolean;
  options: IdName[];
  private _path;
  private _model;

  constructor(private http: HttpClient) {
  }

  get model() {
    return this._model;
  }

  @Input()
  set model(val) {
    if (!this._model && val) {
      this._model = val;

      // Only set the default option when the previous value is undefined (it only occurs the first time).
      this.setDefaultOption();
    } else {
      if (val instanceof Object) {
        this._model = val[this.valueAttr || 'id'];
      } else {
        this._model = val;
      }
    }
  }

  get path() {
    return this._path;
  }

  @Input()
  set path(val) {
    this._path = val;
    this.getOptions();
  }

  async getOptions(): Promise<void> {
    this.options = [];
    try {
      this.options = await this.http.get<IdName[]>(this.path).toPromise();
      this.setDefaultOption();
    } catch (error) {
      console.error(error);
    }
  }

  setDefaultOption(): void {
    if (!this.options || !this.options.length) {
      return;
    }

    if (this.model) {
      // Radio dynamic works with a single value, so we need to get the id if the model set by the input is an object.
      if (this.model instanceof Object) {
        this.model = this.model[this.valueAttr || 'id'];
        if (this.valueAttr) {
          // If the model set by the input is an object but we want a single value in the model, we need to emit it.
          this.onChangeModel(this.model);
        }
      } else if (!this.valueAttr) {
        // If the model set by the input is a single value but we want the object in the model, we need to emit it.
        this.onChangeModel(this.model);
      }

      return;
    }

    const defaultOption = this.options.find(option => option.isDefault);
    this.model = (defaultOption && defaultOption[this.valueAttr || 'id']) || this.options[0][this.valueAttr || 'id'];
    this.onChangeModel(this.model);
  }

  onChangeModel(model): void {
    if (!this.valueAttr) {
      const selectedOption = this.options.find(option => option.id === model);
      this.modelChange.emit(selectedOption);
    } else {
      this.modelChange.emit(model);
    }
  }

}
