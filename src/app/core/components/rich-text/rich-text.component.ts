import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {Editor} from 'primeng/editor';

@Component({
  selector: 'si-rich-text',
  templateUrl: './rich-text.component.html',
  styleUrls: ['./rich-text.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RichTextComponent implements AfterViewInit {
  @Input() text: string;
  @Input() placeholder: string;
  @Input() class: string;
  @Input() id: string;
  @Input() name: string;
  @Input() disabled: boolean;
  @Input() rule: string;
  @Input() model: any;
  @Input() resize: boolean;
  @Output() modelChange = new EventEmitter<string>();
  @ViewChild('textEditor') textEditor: Editor;
  modules = {'toolbar': [['bold', 'italic', 'underline', 'strike', 'clean'], [{'list': 'ordered'}, {'list': 'bullet'}], [{'indent': '-1'}, {'indent': '+1'}]]};

  constructor(private el: ElementRef) {
  }

  ngAfterViewInit() {
    const parentWithClass = this.el.nativeElement.closest('.readmode');

    if (parentWithClass) {
      this.disabled = true;
    }
  }

  onChangeModel(model: any): void {
    // Back webservices do not accept empty model.
    if (model === '') {
      model = undefined;
    }

    this.modelChange.emit(model.htmlValue);
  }
}
