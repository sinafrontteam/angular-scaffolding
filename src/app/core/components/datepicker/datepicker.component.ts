import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Calendar} from 'primeng/calendar';
import {DateFormatPipe} from 'ngx-moment';
import * as moment from 'moment';
import {unitOfTime} from 'moment';

import { DatepickerService} from './datepicker.service';

@Component({
  selector: 'si-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DatepickerComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() id: string;
  @Input() name: string;
  @Input() text: string;
  @Input() class: string;
  @Input() disabled: boolean;
  @Input() rule: string;
  @Input() format = 'L';
  @Input() startOf: string;
  @Input() endOf: string;
  @Input() view = 'date';
  // shows the date and the time in the datepicker
  @Input() showTime: boolean;
  // shows only the time in the datepicker
  @Input() timeOnly: boolean;
  // sets the model empty by default
  @Input() empty: boolean;
  @Output() modelChange = new EventEmitter<any>();
  @ViewChild('pCalendar') pCalendar: Calendar;
  // model of the pCalendar
  modelDate: Date;
  // default date of the pCalendar
  defaultDate: Date;
  yearRange = `${new Date().getFullYear() - 100}:${new Date().getFullYear() + 5}`;
  placeholderTranslated: string;
  localeConfig;
  defaultTimeSetFlag = false;
  private _listeners = [];
  private clearIconElement: HTMLElement;
  // Model of datepicker (milliseconds)
  private _model: number;
  private _placeholder: string;
  private _minDate;
  private _maxDate;
  // The available dates in the pCalendar are from the current day and time.
  private _onlyFuture;
  // The available dates in the pCalendar are until the 23:59 of the current day.
  private _pastOrToday;

  constructor(private renderer: Renderer2, private datepickerService: DatepickerService, private translateService: TranslateService) {
    this.localeConfig = this.datepickerService.getConfig();
  }

  get model() {
    return this._model;
  }

  @Input()
  set model(val) {
    // Assign modelDate of pCalendar.
    if (!val) {
      this.modelDate = null;
      this.displayClearIcon(false);
    } else {
      this.modelDate = this.setTimeInDate(this.parseMsToDate(val));
      this.displayClearIcon();
    }

    // Assign model of datepicker and emit new value when previous model is undefined (because maybe the time is different).
    // Example: when datepicker is initialized and received a date as a model, we need to emit its value with the correct time.
    if (!this._model && val || (val && val !== this._model)) {
      this._model = this.parseDateToMs(this.setTimeInDate(this.parseMsToDate(val)));
      this.modelChange.emit(this._model);
    } else {
      this._model = val;
    }
  }

  get placeholder() {
    return this._placeholder;
  }

  @Input()
  set placeholder(val) {
    this._placeholder = val;
    this.translateService.get(val).toPromise().then(translation => {
      this.placeholderTranslated = translation;
    });
  }

  get minDate() {
    return this._minDate;
  }

  @Input()
  set minDate(val) {
    if (!val) {
      this._minDate = val;
    } else {
      this._minDate = this.parseMsToDate(val);
    }
  }

  get maxDate() {
    return this._maxDate;
  }

  @Input()
  set maxDate(val) {
    if (!val) {
      this._maxDate = val;
    } else {
      this._maxDate = this.parseMsToDate(val);
    }
  }

  get onlyFuture() {
    return this._onlyFuture;
  }

  @Input()
  set onlyFuture(val) {
    this._onlyFuture = val;
    this.minDate = new Date().getTime();
  }

  get pastOrToday() {
    return this._pastOrToday;
  }

  @Input()
  set pastOrToday(val) {
    this._pastOrToday = val;
    this.maxDate = new Date(moment().endOf('day').valueOf()).getTime();
  }

  ngOnInit() {
    this.decorateCalendar();
    // Set default time in the calendar dropdown.
    this.defaultDate = this.setTimeInDate(new Date());
    // Set today in the model by default.
    if (this.empty === undefined && !this.model) {
      this.modelDate = this.defaultDate;
      if (!this.model) {
        this.modelChange.emit(this.parseDateToMs(this.modelDate));
      }
    }
  }

  ngAfterViewInit() {
    this.setClearIcon();
    // If the model comes with a date, it is set before ngAfterViewInit, so we have to check it to display the clear icon after creating it.
    if (this.modelDate) {
      this.displayClearIcon();
    }
  }

  setClearIcon(): void {
    this.clearIconElement = this.renderer.createElement('i');
    this.renderer.addClass(this.clearIconElement, 'icon-closex');
    this.renderer.addClass(this.clearIconElement, 'display-none');

    const pCalendarElement = this.pCalendar.el.nativeElement;
    const uiCalendarElement = pCalendarElement.querySelector('span.ui-calendar');
    const uiCalendarIconElement = pCalendarElement.querySelector('.ui-calendar-button');
    this.renderer.insertBefore(uiCalendarElement, this.clearIconElement, uiCalendarIconElement);
    this._listeners.push(
      this.renderer.listen(this.clearIconElement, 'click', (event) => this.onClickClearIcon(event))
    );
  }

  displayClearIcon(displayFlag = true): void {
    if (!this.clearIconElement) {
      return;
    }

    if (displayFlag) {
      this.renderer.removeClass(this.clearIconElement, 'display-none');
    } else {
      this.renderer.addClass(this.clearIconElement, 'display-none');
    }
  }

  onSelectDate(date): void {
    this.modelChange.emit(this.parseDateToMs(this.setTimeInDate(date)));
  }

  onCloseCalendar(): void {
    if (this.modelDate) {
      this.displayClearIcon();
    } else {
      this.modelChange.emit(null);
      this.displayClearIcon(false);
    }
  }

  onChangeInput(event): void {
    event.target.value = this.setSeparatorInStringDate(event.target.value);
    if (event.target.value) {
      this.displayClearIcon();
      let date = this.pCalendar.parseDate(event.target.value, this.format);
      date = this.validateDate(date);
      this.modelChange.emit(this.parseDateToMs(this.setTimeInDate(date)));
    } else {
      this.displayClearIcon(false);
      this.modelChange.emit(null);
    }
  }

  onClickClearIcon(event): void {
    this.displayClearIcon(false);
    this.pCalendar.onClearButtonClick(event);
    this.modelChange.emit(null);
  }

  validateDate(date: Date): Date {
    if (!date || (this.minDate && this.minDate > date) || (this.maxDate && this.maxDate < date)) {
      return;
    }

    return date;
  }

  parseDateToMs(date: Date): number {
    if (!date) {
      return;
    }

    return date.getTime();
  }

  parseMsToDate(dateMs: number): Date {
    if (!dateMs) {
      return;
    }

    return new Date(dateMs);
  }

  // Set default time with startOf, endOf.
  setTimeInDate(date: Date) {
    if (!date) {
      return;
    }

    // Only date: (!this.showTime && !this.timeOnly)
    // Date + time: (this.showTime && !this.defaultTimeSetFlag)
    // Only time: (this.timeOnly && !this.defaultTimeSetFlag)
    if (this.pCalendar && (this.startOf || this.endOf) &&
      ((!this.showTime && !this.timeOnly) || (this.showTime && !this.defaultTimeSetFlag) || (this.timeOnly && !this.defaultTimeSetFlag))) {

      this.defaultTimeSetFlag = true;

      if (this.startOf) {
        date = new Date(moment(date).startOf(this.startOf as unitOfTime.StartOf).valueOf());
      } else if (this.endOf) {
        date = new Date(moment(date).endOf(this.endOf as unitOfTime.StartOf).valueOf());
      }
    }

    return date;
  }

  setSeparatorInStringDate(date: string) {
    if (this.timeOnly) {
      return date.replace(/^(\d\d)(\d)$/g, '$1:$2');
    } else if (this.showTime) {
      return date
        .replace(/^(\d\d)(\d)$/g, '$1/$2')
        .replace(/^(\d\d\/\d\d)(\d+)$/g, '$1/$2')
        .replace(/^(\d\d\/\d\d\/\d\d\d\d\s\d\d)(\d+)$/g, '$1:$2')
        .replace(/[^\d\/\s\:]/g, ''); // TODO no deja borrar cuando has terminado de escribir todo
    } else if (this.format.length > 4 && this.format.length < 8) {
      return date
        .replace(/^(\d\d)(\d)$/g, '$1/$2')
        .replace(/[^\d\/]/g, '');
    } else {
      return date
        .replace(/^(\d\d)(\d)$/g, '$1/$2')
        .replace(/^(\d\d\/\d\d)(\d+)$/g, '$1/$2')
        .replace(/[^\d\/]/g, '');
    }
  }

  // Functions overwritting the real ones in the Calendar component.
  async decorateCalendar() {
    if (!this.pCalendar) {
      return;
    }

    // GetDateFormat overwritten to get the format set in Datepicker component.
    this.pCalendar.getDateFormat = () => {
      return this.format;
    };

    // FormatDate overwritten because the pCalendar does not accept localized formats like 'L'.
    const dateFormatPipe = new DateFormatPipe();
    this.pCalendar.formatDate = (date) => dateFormatPipe.transform(date, this.format);

    // FormatDateTime overwritten because the pCalendar append the time again after formatting the date.
    this.pCalendar.formatDateTime = function (date) {
      let formattedValue = null;
      if (date) {
        if (this.timeOnly) {
          formattedValue = this.formatTime(date);
        } else {
          formattedValue = this.formatDate(date);
        }
      }

      return formattedValue;
    };

    // ParseDateTime overwritten because we use own formats as 'L' or 'L LT', which are not supported by pCalendar.
    this.pCalendar.parseDateTime = function (text) {
      let date;
      const parts = text.split(' ');
      if (this.timeOnly) {
        date = new Date();
        this.populateTime(date, parts[0], parts[1]);
      } else {
        const dateFormat = this.getDateFormat();
        date = this.parseDate(text, dateFormat);
      }

      return date;
    };

    // ParseDate overwritten because we use own formats as 'L' or 'L LT', which are not supported by pCalendar.
    this.pCalendar.parseDate = (stringDate, format) => {
      if (moment(stringDate, format, true).isValid()) {
        return new Date(moment(stringDate, format, true).valueOf());
      }

      return null;
    };
  }

  ngOnDestroy() {
    this._listeners.forEach(fn => fn());
  }

}
