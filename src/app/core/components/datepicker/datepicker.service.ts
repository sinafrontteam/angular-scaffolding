import {Injectable} from '@angular/core';
import {localeKeyLong} from '../../constants/i18n.constants';
import {TranslateService} from '@ngx-translate/core';

const config = {};
config[localeKeyLong['ES']] = {
  firstDayOfWeek: 1,
  dayNames: ['domingo', 'lunes', 'martes', 'mi\xe9rcoles', 'jueves', 'viernes', 's\xe1bado'],
  dayNamesShort: ['dom', 'lun', 'mar', 'mi\xe9', 'jue', 'vie', 's\xe1b'],
  dayNamesMin: ['DO', 'LU', 'MA', 'MI', 'JU', 'VI', 'S\xc1'],
  monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
  monthNamesShort: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
  today: 'Hoy',
  clear: 'Borrar'
};

config[localeKeyLong['ENGB']] = {
  firstDayOfWeek: 0,
  dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
  dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
  dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
  monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
  monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  today: 'Today',
  clear: 'Clear'
};

@Injectable({
  providedIn: 'root'
})
export class DatepickerService {

  constructor(private translate: TranslateService) {
  }

  getConfig() {
    const language = this.translate.getDefaultLang();
    if (!config[language]) {
      return config[localeKeyLong.DEFAULT];
    }

    return config[language];
  }

}
