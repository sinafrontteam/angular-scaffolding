import {
  AfterContentInit,
  Component,
  ContentChild,
  ContentChildren,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  Renderer2,
  ViewEncapsulation
} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {DomHandler} from 'primeng/api';
import {Table, TableCheckbox} from 'primeng/components/table/table';
import {TranslateService} from '@ngx-translate/core';
import {LoaderService} from '../../services/loader.service';
import {DispatcherService} from '../../services/dispatcher.service';
import {of, Subject} from 'rxjs';
import {catchError, switchMap} from 'rxjs/operators';

const tableDefaultConfig = {
  paginator: true,
  rows: 20,
  rowsPerPageOptions: [5, 10, 20, 50, 75, 100],
  resizableColumns: true,
  lazy: true
};

@Component({
  selector: 'si-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TableComponent implements OnInit, OnDestroy, AfterContentInit {
  @Input() id: string;
  @Input() path: string;
  @Input() frozen: boolean;
  // If it is true, the table sets the horizontal scroll to the end.
  @Input() scrollEnd: boolean;
  // If the http response has config attribute, we need to know the key of the table model in the body response.
  @Input() pathConfigBodyAttribute: string;
  // If the http response has config attribute, we need to emit that config.
  @Output() pathConfigChange = new EventEmitter<any>();
  @Output() modelChange = new EventEmitter<any[]>();
  @ContentChild('pTable') pTable: Table;
  @ContentChildren(TableCheckbox, {descendants: true}) pTableCheckboxes: QueryList<TableCheckbox>;
  lastLazyLoadData;
  reloadTableSubscription: number;
  // Indicates if there is user activity or not.
  activeDom = true;
  // Variables to manage the http request observable.
  httpRequest$ = new Subject();
  lastHttpParams: HttpParams;
  lastFirst: number;
  lastRows: number;
  isLoading = false;
  rowsText: string;
  ofText: string;
  private _model: any[];
  private paginationInfoElement: HTMLElement;
  private horizontalScrollEndDone = false;

  // If the reset variable is initialized with 0, the table does NOT get the items for the first time. Otherwise, it DOES.
  private _reset: number;
  private _reload: number;
  private _reloadInterval: number;
  private _lazy: boolean;

  constructor(private http: HttpClient,
              private loaderService: LoaderService,
              private renderer: Renderer2,
              private translateService: TranslateService,
              private dispatcher: DispatcherService) {
  }

  get model() {
    return this._model;
  }

  @Input()
  set model(val) {
    this._model = val;
    if (!val || !val.length) {
      this.pTable.value = [];
      this.pTable.totalRecords = 0;
      this.lastFirst = 0;
      this.setPaginationInfo(this.pTable.totalRecords, this.lastFirst, this.pTable.rows);
    }
  }

  get reset() {
    return this._reset;
  }

  @Input()
  set reset(val) {
    this._reset = val;
    if (val) {
      setTimeout(() => {
        this.unselectAllItems();
        this.getItems();
        this.pTable.first = 0;
      });
    }
  }

  get reload() {
    return this._reload;
  }

  @Input()
  set reload(val) {
    this._reload = val;
    if (val) {
      this.reloadTable();
    }
  }

  get reloadInterval() {
    return this._reloadInterval;
  }

  @Input()
  set reloadInterval(val) {
    this._reloadInterval = val;
    if (val) {
      this.reloadTableSubscription = setInterval(() => this.reloadTable(true), val);
    }
  }

  get lazy() {
    return this._lazy;
  }

  @Input()
  set lazy(val) {
    this._lazy = val;
    this.pTable.lazy = val;
    if (!val) {
      setTimeout(() => {
        this.setPaginationInfo(this.pTable.value.length, 0, this.pTable.rows);
      });
    }
  }

  ngOnInit() {
    this.decorateTable();
    this.getTranslations();
    this.initHttpRequestObservable();

    // If there are more than one table in the same page, they need an id to be differentiated.
    const observerKey = 'tableComponent' + this.id;
    this.dispatcher.register('idle-time-inactivity', () => this.onActivateDOM(false), observerKey);
    this.dispatcher.register('idle-time-activity', () => this.onActivateDOM(true), observerKey);
  }

  async getTranslations(): Promise<void> {
    this.rowsText = await this.translateService.get('CORE_ROWS').toPromise();
    this.ofText = await this.translateService.get('CORE_OF').toPromise();
  }

  initHttpRequestObservable(): void {
    if (!this.path) {
      return;
    }

    this.httpRequest$.pipe(
      switchMap(() => {
        return this.http.get(this.path, {params: this.lastHttpParams, observe: 'response'}).pipe(
          catchError(e => {
            return of(e);
          })
        );
      })
    ).subscribe(response => {
      if (response['error']) {
        this.processHttpErrorResponse(response);
      } else {
        this.processHttpResponse(response);
      }
    });
  }

  ngAfterContentInit() {
    this.setConfig();
    this.pTable.onLazyLoad.subscribe((data) => {
      this.unselectAllItems();
      this.onLazyLoad(data);
    });
    this.pTable.onPage.subscribe((data) => this.onPage(data));
  }

  onActivateDOM(activateFlag = true): void {
    this.activeDom = activateFlag;
  }

  setConfig(): void {
    this.pTable.paginator = this.pTable.paginator !== undefined ? this.pTable.paginator : tableDefaultConfig.paginator;
    this.pTable.rows = this.pTable.rows !== undefined ? this.pTable.rows : tableDefaultConfig.rows;
    this.pTable.rowsPerPageOptions = this.pTable.rowsPerPageOptions !== undefined ? this.pTable.rowsPerPageOptions : tableDefaultConfig.rowsPerPageOptions;
    this.pTable.resizableColumns = this.pTable.resizableColumns !== undefined ? this.pTable.resizableColumns : tableDefaultConfig.resizableColumns;
    if (this.lazy === undefined) {
      this.pTable.lazy = tableDefaultConfig.lazy;
      this.lazy = tableDefaultConfig.lazy;
    } else {
      this.pTable.lazy = this.lazy;
    }

    if (this.frozen) {
      this.pTable.scrollable = true;
      this.pTable.scrollHeight = `${window.innerHeight * 0.65}px`;
    }
  }

  setHorizontalScrollEnd(): void {
    if (!this.frozen || this.horizontalScrollEndDone) {
      return;
    }

    setTimeout(() => {
      const scrollableBodyElement = this.pTable.el.nativeElement.querySelector('.ui-table-unfrozen-view .ui-table-scrollable-body');
      const scrollWidth = scrollableBodyElement.scrollWidth;
      const clientWidth = scrollableBodyElement.clientWidth;
      scrollableBodyElement.scrollLeft = scrollWidth - clientWidth;
      this.horizontalScrollEndDone = true;
    });
  }

  onLazyLoad(data): void {
    this.lastLazyLoadData = data;
    const page = ((data.first / data.rows) + 1).toString();
    let sort = data.sortField;
    if (data.sortOrder === -1) {
      sort = `-${sort}`;
    }

    this.getItems(page, null, sort, data.first, data.rows);
  }

  async onPage(data): Promise<void> {
    if (!this.lazy) {
      await this.setPaginationInfo(this.pTable.value.length, data.first, data.rows);
    }
  }

  getItems(page = '1', size?: string, sort?: string, first = 0, rows?: number): void {
    if (!this.isLoading) {
      this.loaderService.start();
      this.isLoading = true;
    }

    let httpParams = new HttpParams();
    if (this.pTable.paginator) {
      httpParams = httpParams
        .set('page', page)
        .set('size', size || this.pTable.rows.toString());
    }

    if (sort) {
      httpParams = httpParams.set('sort', sort);
    }

    this.lastHttpParams = httpParams;
    this.lastFirst = first;
    this.lastRows = rows;

    this.getHttpRequest();
  }

  getHttpRequest(): void {
    this.httpRequest$.next();
  }

  async processHttpErrorResponse(response: HttpResponse<any>): Promise<void> {
    this.pTable.value = [];
    this.pTable.totalRecords = 0;
    await this.setPaginationInfo(this.pTable.totalRecords, this.lastFirst, this.lastRows || this.pTable.rows);
    this.loaderService.stop();
    this.isLoading = false;
  }

  async processHttpResponse(response: HttpResponse<any>): Promise<void> {
    if (this.pathConfigBodyAttribute) {
      this.pTable.value = <any[]>response.body[0][this.pathConfigBodyAttribute];
      this.pathConfigChange.emit(response.body[0]['config']);
    } else {
      this.pTable.value = <any[]>response.body;
      this.model = this.pTable.value;
      this.modelChange.emit(this.model);
    }

    this.pTable.totalRecords = parseInt(response.headers.get('x-total-count'), 10);

    await this.setPaginationInfo(this.pTable.totalRecords, this.lastFirst, this.lastRows || this.pTable.rows);
    if (this.scrollEnd) {
      this.setHorizontalScrollEnd();
    }

    this.loaderService.stop();
    this.isLoading = false;
  }

  async setPaginationInfo(totalRows, originalFirst, rows): Promise<void> {
    if (!this.pTable.paginator) {
      return;
    }

    const firstRow = originalFirst + 1;
    let lastRow = originalFirst + rows;
    lastRow = lastRow < totalRows ? lastRow : totalRows;

    if (!this.paginationInfoElement) {
      const pTableElement = this.pTable.el.nativeElement;
      const uiPaginatorElement = pTableElement.querySelector('.ui-paginator');
      const uiDropdownElement = uiPaginatorElement.querySelector('.ui-dropdown');

      // Create rows text.
      const rowsTextElement = this.renderer.createElement('div');
      const paginationRowsTextElement = this.renderer.createText(this.rowsText);
      rowsTextElement.appendChild(paginationRowsTextElement);
      this.renderer.insertBefore(uiDropdownElement.parentElement, rowsTextElement, uiDropdownElement);

      // Create pagination info container.
      this.paginationInfoElement = this.renderer.createElement('div');
      this.renderer.appendChild(uiDropdownElement.parentElement, this.paginationInfoElement);
    } else {
      this.renderer.removeChild(this.paginationInfoElement, this.paginationInfoElement.firstChild);
    }

    const paginationInfoText = `${firstRow}-${lastRow} ${this.ofText} ${totalRows}`;
    const paginationInfoTextElement = this.renderer.createText(paginationInfoText);
    this.paginationInfoElement.appendChild(paginationInfoTextElement);
  }

  unselectAllItems(): void {
    if (this.pTable.selection) {
      this.pTable.selection = null;
      this.pTable.selectionChange.emit(null);
    }
  }

  reloadTable(intervalReloading?: boolean): void {
    if (intervalReloading && this.activeDom) {
      return;
    }

    if (this.lastLazyLoadData) {
      this.onLazyLoad(this.lastLazyLoadData);
    } else {
      this.getItems();
    }
  }

  // onMouseEnter and onMouseLeave are functions used when we have a frozen table with row selection.
  onMouseEnter(rowData): void {
    rowData.hover = true;
  }

  onMouseLeave(rowData): void {
    rowData.hover = false;
  }

  // Functions overwriting the real ones in the Table component.
  async decorateTable(): Promise<void> {
    if (!this.pTable) {
      return;
    }

    // HandleRowClick overwritten because we don't want the row to be selected when we click on it. Just emit the event.
    this.pTable.handleRowClick = function (event) {
      // Don't emit the event in this cases.
      const target = (<HTMLElement> event.originalEvent.target);
      const targetNode = target.nodeName;
      const parentNode = target.parentElement.nodeName;
      if (targetNode === 'INPUT' || targetNode === 'BUTTON' || targetNode === 'A' || targetNode === 'I' ||
        parentNode === 'INPUT' || parentNode === 'BUTTON' || parentNode === 'A' || parentNode === 'I' ||
        (DomHandler.hasClass(event.originalEvent.target, 'si-table_row_toggler')) ||
        target.closest('.ui-clickable')) {
        return;
      }

      this.onRowSelect.emit({
        originalEvent: event.originalEvent,
        data: event.rowData,
        type: 'row',
        index: event.rowIndex
      });
    };

    // ToggleRowWithCheckbox overwritten to stop the event propagation so that the row isn't clicked.
    const originalToggleRowWithCheckbox = this.pTable.toggleRowWithCheckbox.bind(this.pTable);
    this.pTable.toggleRowWithCheckbox = (event, rowData) => {
      event.originalEvent.stopPropagation();
      event.originalEvent.preventDefault();
      originalToggleRowWithCheckbox(event, rowData);
    };

    // ToggleRowsWithCheckbox overwritten to not check disabled checkboxes.
    this.pTable.toggleRowsWithCheckbox = (event, check) => {
      this.pTable._selection = check ? this.pTableCheckboxes.filter(checkbox => !checkbox.disabled).map(checkbox => checkbox.value) : [];
      this.pTable.preventSelectionSetterPropagation = true;
      this.pTable.updateSelectionKeys();
      this.pTable.selectionChange.emit(this.pTable._selection);
      this.pTable.tableService.onSelectionChange();
      this.pTable.onHeaderCheckboxToggle.emit({originalEvent: event, checked: check});

      if (this.pTable.isStateful()) {
        this.pTable.saveState();
      }
    };
  }

  ngOnDestroy() {
    const observerKey = 'tableComponent' + this.id;
    this.dispatcher.unregisterFunction('idle-time-inactivity', observerKey);
    this.dispatcher.unregisterFunction('idle-time-activity', observerKey);
    if (this.reloadTableSubscription) {
      clearInterval(this.reloadTableSubscription);
    }

    this.httpRequest$.complete();
  }
}
