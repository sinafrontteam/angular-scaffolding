import {
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {SelectItem} from 'primeng/api';
import {Dropdown} from 'primeng/dropdown';

import {keyboardKeys} from '../../constants/keyboard-keys.constants';
import {IdName} from '../../models/id-name.model';
import {ValidationElementComponent} from '../validation-element/validation-element.component';

const DEFAULT_SIZE = '50';

@Component({
  selector: 'si-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SelectorComponent implements OnInit, OnDestroy {
  @Input() id: string;
  @Input() name: string;
  @Input() text: string;
  @Input() class: string;
  @Input() disabled: boolean;
  @Input() rule: string;
  @Input() labelAttr = 'name';
  @Input() valueAttr: string;
  @Input() tooltipPosition: string;
  @Input() tooltipText: string;
  @Input() tooltipIcon: string;
  @Input() otherValue = false;
  @Input() backSearchParam = 'q';
  @Input() hideLabel = false;

  // Needed if the component is being used in AngularJS view and it needs validation.
  @Input() ng1Form: any;
  @Input() showClear = true;
  @Output() modelChange = new EventEmitter<any>();
  @Output() selectDefaultChange = new EventEmitter<any>();
  @ViewChild('pDropdown') pDropdown: Dropdown;
  @ViewChild('siSelectorLoader') siSelectorLoader: ElementRef;
  @ViewChild('validationElement') validationElement: ValidationElementComponent;
  @ContentChild('selectedItem') selectedItem: TemplateRef<ElementRef>;
  @ContentChild('item') item: TemplateRef<ElementRef>;
  options: SelectItem[];
  placeholderTranslated: string;
  inputFilterSubject = new Subject<string>();
  totalRecords: number;
  otherText: string;
  private _model: IdName;
  private _placeholder: string;
  private _values: object[];
  private _path: string;
  private _backSearch = false;
  private _selectDefault = false;

  constructor(private translateService: TranslateService,
              private http: HttpClient,
              private renderer: Renderer2) {
    this.getOtherLabelTranslation();
  }

  get model() {
    return this._model;
  }

  @Input()
  set model(val: IdName) {
    // When the component has valueAttr, the model emitted only has the parameter specified in valueAttr.
    // So, we do not want to update the internal model when the model is updated with the valueAttr parameter only.
    if (this.valueAttr && this._model && val && !(val instanceof Object) && val === this._model[this.valueAttr]) {
      return;
    }

    if (!val || val instanceof Object) {
      this._model = this.checkModelOtherValue(val);
    } else {
      this._model = null;
    }

    setTimeout(() => {
      this.loadDefaultModel(val || this._model);
    });
  }

  get placeholder() {
    return this._placeholder;
  }

  @Input()
  set placeholder(val) {
    this._placeholder = val;
    this.translateService.get(val).toPromise().then(translation => {
      this.placeholderTranslated = translation;
    });
  }

  get values() {
    return this._values;
  }

  @Input()
  set values(val) {
    if (val) {
      this.setValuesAsync(val);
    }
  }

  get backSearch() {
    return this._backSearch;
  }

  @Input()
  set backSearch(val) {
    // Backsearch is true when val is true or when val is ''.
    if (val === false) {
      this._backSearch = false;
    } else {
      this._backSearch = true;
    }
  }

  get path() {
    return this._path;
  }

  @Input()
  set path(val) {
    this._path = val;
    this.options = null;
    // If another field changes, the path changes. So, we need to check if the option selected is valid and update it if it is necessary.
    this.setModelValid();
  }

  @Input()
  set selectFirst(val) {
    this.selectFirstOption();
  }

  @Input()
  set selectSingle(val) {
    this.selectIfSingleOption();
  }

  get selectDefault() {
    return this._selectDefault;
  }

  @Input()
  set selectDefault(val) {
    this._selectDefault = true;
    this.getOptions();
  }

  async setValuesAsync(val): Promise<void> {
    const valuesTranslated = await this.translateValues(val);
    this._values = valuesTranslated;
    this.options = this.mapOptionsFromBack(valuesTranslated);
  }

  ngOnInit() {
    this.decorateDropdown();

    this.inputFilterSubject.asObservable()
      .pipe(debounceTime(400), distinctUntilChanged())
      .subscribe(() => {
        if (this.backSearch && this.pDropdown.filterValue && this.pDropdown.filterValue.length < 3) {
          return;
        }

        this.getOptions();
      });
  }

  onChangeDropdownOption({originalEvent, value}): void {
    this.emitModel(value);
  }

  async onClickDropdown(): Promise<void> {
    if (this.pDropdown.overlayVisible) {
      return;
    }

    await this.getOptions();
  }

  onHideDropdown(): void {
    if (this.backSearch) {
      this.options = [];
    }
  }

  async getOptions(searchModelName?: string): Promise<void> {
    if (this.options && !this.backSearch) {
      return;
    }

    let httpParams = new HttpParams();
    if (this.backSearch) {
      httpParams = httpParams
        .set('size', DEFAULT_SIZE)
        .set(this.backSearchParam, this.pDropdown.filterValue || searchModelName || '');

      // Empty the options because they are going to be loaded again. Apart from that, we need to empty them
      // to set the total count div correctly.
      this.options = [];
    }

    this.renderer.addClass(this.siSelectorLoader.nativeElement, 'loading');

    try {
      const response = await this.http.get(this.path, {params: httpParams, observe: 'response'}).toPromise();
      this.options = this.mapOptionsFromBack(<IdName[]>response.body);

      if (this.backSearch && response.headers.get('x-total-count')) {
        this.totalRecords = parseInt(response.headers.get('x-total-count'), 10);
        await this.setTotalCountElement(this.options.length, this.totalRecords);
      }

      await this.addOtherValue();

      // Align the overlay element again after loading the options.
      this.pDropdown.alignOverlay();
    } catch (error) {
      console.error(error);
    }

    this.renderer.removeClass(this.siSelectorLoader.nativeElement, 'loading');
  }

  mapOptionsFromBack(options: IdName[]): SelectItem[] {
    if (!options || !options.length) {
      return [];
    }

    return options.map(option => {
      const optionParsed = {
        label: option[this.labelAttr],
        value: option
      };

      if (this.selectDefault && option.isDefault) {
        this.setOptionOfOptions(optionParsed);
        this.modelChange.emit(this.model);
        this.selectDefaultChange.emit();
      }

      return optionParsed;
    });
  }

  async getOtherLabelTranslation(): Promise<void> {
    this.otherText = await this.translateService.get('CORE_OTHER').toPromise();
  }

  // Check if the model has id -1 to update it with the name.
  checkModelOtherValue(model: IdName): IdName {
    if (!this.otherValue || !model || model.id !== -1 || model[this.labelAttr]) {
      return model;
    }

    const otherObject = {
      id: -1,
      [this.labelAttr]: this.otherText
    };

    this.emitModel(otherObject);

    return otherObject;
  }

  // Add 'Other' option to options array.
  addOtherValue(): void {
    if (!this.otherValue) {
      return;
    }

    if (!this.options) {
      this.options = [];
    }

    const otherOption = {
      label: this.otherText,
      value: {
        id: -1,
        [this.labelAttr]: this.otherText
      }
    };

    this.options.push(otherOption);
  }

  async translateValues(values): Promise<IdName[]> {
    for (let i = 0; i < values.length; i++) {
      values[i][this.labelAttr] = await this.translateService.get(values[i][this.labelAttr]).toPromise();
    }

    return values;
  }

  emitModel(model: IdName): void {
    if (this.valueAttr && model) {
      this.modelChange.emit(model[this.valueAttr]);
    } else {
      this.modelChange.emit(model);
    }
  }

  // Set a option obtained of options in pDropdown and selector model.
  setOptionOfOptions(option: SelectItem): void {
    if (!option) {
      return;
    }

    this.pDropdown.selectedOption = option;
    // Assign the value too because if not, dropdown component deletes the selectedOption.
    this.pDropdown.value = option.value;

    // Emit the model when it is loaded with an id but it does not have valueAttr, what means we want the whole object.
    if (!(this._model instanceof Object) && !this.valueAttr) {
      this.emitModel(option.value);
    }

    this._model = option.value;
  }

  // Load a default model passed by parameter. It works like the old af-select-it.
  // It is not possible to load a default model if the selector has backSearch and valueAttr attributes.
  async loadDefaultModel(val: IdName): Promise<void> {
    if (val) {
      // If val is an object, set it as normal.
      // Else, it has to be an id. So, we have to retrieve the options, find the object by the id and set it in selectedOption.
      if (val instanceof Object) {
        const optionToSelect = {label: val[this.labelAttr], value: val};
        this.setOptionOfOptions(optionToSelect);
      } else {
        if (!this.pDropdown.options || !this.pDropdown.options.length) {
          await this.getOptions();
        }

        if (this.options) {
          const optionToSelect = this.options.find(option => option.value[this.valueAttr || 'id'] == val);
          this.setOptionOfOptions(optionToSelect);
        }
      }
    }
  }

  // Get the options and select the first one. It works like the old af-select-first.
  async selectFirstOption(): Promise<void> {
    await this.getOptions();
    if (!this.options.length) {
      return;
    }

    this.setOptionOfOptions(this.options[0]);

    //We need to emit the model when we have valueAttr.
    if (this.model instanceof Object && this.valueAttr) {
      this.emitModel(this.model);
    }
  }

  // Get the options and select the single one if there is only one option. It works like the old af-select-single.
  async selectIfSingleOption(): Promise<void> {
    await this.getOptions();
    if (this.options.length !== 1) {
      return;
    }

    this.setOptionOfOptions(this.options[0]);

    //We need to emit the model when we have valueAttr.
    if (this.model instanceof Object && this.valueAttr) {
      this.emitModel(this.model);
    }
  }

  // Check if the model is in the options and if not, remove it.
  async setModelValid(): Promise<void> {
    if (!this.model && !this.validationElement.showRequiredElementFlag) {
      return;
    }

    if (!this.options || !this.options.length) {
      // If the selector has backSearch attribute we need to get the options filtering by the name of the model.
      if (this.backSearch && !this.valueAttr) {
        await this.getOptions(this.model[this.labelAttr]);
      } else {
        await this.getOptions();
      }
    }

    // Set the model if the selector is required and there is only one option to select.
    // Don't assign and emit the model if it is already assigned in the model.
    if (this.validationElement.showRequiredElementFlag && this.options.length === 1 &&
      (!this.model || (this.valueAttr && this.model[this.valueAttr] !== this.options[0].value[this.valueAttr]) ||
        (!this.valueAttr && this.model.id !== this.options[0].value['id']))) {
      this.setOptionOfOptions(this.options[0]);
      this.emitModel(this.model);
      return;
    }

    if (this.model && this.options && this.options.length) {
      const isModelValid = this.options.some(option => option.value[this.valueAttr || 'id'] === (this.model[this.valueAttr || 'id'] || this.model));
      if (!isModelValid) {
        this.model = null;
        this.emitModel(this.model);
      }
    }
  }

  // Create total count element
  async setTotalCountElement(records: number, totalRecords: number): Promise<void> {
    // Set dropdown element
    const dropdownPanelElement = this.pDropdown.el.nativeElement.querySelector('.ui-dropdown-panel');
    let totalCountElement = this.pDropdown.el.nativeElement.querySelector('.si-selector_total-count');

    // Translate texts
    const totalCountText = await this.translateService
      .get('CORE_SHOWING_RESULTS_OF', {records, totalRecords}).toPromise();

    if (dropdownPanelElement) {
      if (!totalCountElement) {
        // Create totalCount element
        totalCountElement = this.renderer.createElement('div');
        totalCountElement.classList.add('si-selector_total-count');
        this.renderer.appendChild(dropdownPanelElement, totalCountElement);
      }
      this.renderer.setProperty(totalCountElement, 'innerHTML', totalCountText);
    }
  }

  // Functions overwriting the real ones in the Dropdown component.
  async decorateDropdown(): Promise<void> {
    if (this.pDropdown) {
      // ActivateFilter overwritten because the pDropdown only filter in front.
      // So, we implement the inputFilterSubject to filter in back.
      const originalActivateFilter = this.pDropdown.activateFilter.bind(this.pDropdown);
      this.pDropdown.activateFilter = () => {
        if (!this.backSearch) {
          originalActivateFilter();
          return;
        }

        this.inputFilterSubject.next(this.pDropdown.filterValue);
      };

      // OnFilter overwritten because when the filter is in back we need to filter even when the inputValue is null.
      this.pDropdown.onFilter = (event) => {
        const inputValue = event.target.value;
        if (this.backSearch || inputValue && inputValue.length) {
          this.pDropdown.filterValue = inputValue;
          this.pDropdown.activateFilter();
        } else {
          this.pDropdown.filterValue = null;
          this.pDropdown.optionsToDisplay = this.options;
        }
      };

      // UpdateSelectedOption overwritten because we want the model to be assigned even when the model is not in the options.
      // Example: when the model is received by the input.
      this.pDropdown.updateSelectedOption = function (val) {
        if (!val) {
          this.selectedOption = null;
        }

        if (this.autoDisplayFirst && !this.placeholder && !this.selectedOption && this.optionsToDisplay && this.optionsToDisplay.length && !this.editable) {
          this.selectedOption = this.optionsToDisplay[0];
        }

        this.selectedOptionUpdated = true;
      };

      // AlignOverlay overwritten to modify the top pixels of the overlay element because if not, the blue animation cannot be seen.
      const originalAlignOverlay = this.pDropdown.alignOverlay.bind(this.pDropdown);
      this.pDropdown.alignOverlay = function () {
        setTimeout(() => {
          originalAlignOverlay();
          if (this.overlay) {
            const top = this.overlay.style.top;
            this.overlay.style.top = (parseInt(top.substring(0, top.length - 2), 10) - 2).toString() + 'px';
          }
        });
      };
    }
  }

  @HostListener('keyup', ['$event'])
  async keyEvent(event: KeyboardEvent) {
    if (event.key === keyboardKeys.TAB) {
      this.pDropdown.onMouseclick(event);
    }
  }

  ngOnDestroy() {
    this.inputFilterSubject.complete();
  }

}
