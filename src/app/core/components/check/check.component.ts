import {Component, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {keyboardKeys} from '../../constants/keyboard-keys.constants';

@Component({
  selector: 'si-check',
  templateUrl: './check.component.html',
  styleUrls: ['./check.component.scss']
})
export class CheckComponent {
  @Input() text: string;
  @Input() class: string;
  @Input() id: string;
  @Input() name: string;
  @Input() model: any;
  @Output() modelChange = new EventEmitter<any>();
  @Input() disabled: boolean;
  private _checked;

  constructor() {
  }

  get checked() {
    return this._checked;
  }

  @Input()
  set checked(val) {
    if (val || val === '') {
      this.model = true;
    } else {
      this.model = false;
    }

    this.onChangeModel(this.model);
  }

  onChangeModel(model) {
    this.modelChange.emit(model);
  }

  @HostListener('keydown', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.key === keyboardKeys.ENTER) {
      this.model = !this.model;
      this.onChangeModel(this.model);
    }
  }
}
