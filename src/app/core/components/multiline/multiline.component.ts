import {Component, EventEmitter, Input, Output, ElementRef, AfterViewInit} from '@angular/core';

@Component({
  selector: 'si-multiline',
  templateUrl: './multiline.component.html',
  styleUrls: ['./multiline.component.scss']
})
export class MultilineComponent implements AfterViewInit {
  @Input() text: string;
  @Input() placeholder: string;
  @Input() class: string;
  @Input() id: string;
  @Input() name: string;
  @Input() disabled: boolean;
  @Input() rule: string;
  @Input() model: string;
  @Input() rows: number;
  @Output() modelChange = new EventEmitter<string>();

  constructor(private el: ElementRef) {
  }

  ngAfterViewInit() {
    const parentWithClass = this.el.nativeElement.closest('.readmode');

    if (parentWithClass) {
      this.disabled = true;
    }
  }

  onChangeModel(model: string): void {
    // Back webservices do not accept empty model.
    if (model === '') {
      model = undefined;
    }

    this.modelChange.emit(model);
  }
}
