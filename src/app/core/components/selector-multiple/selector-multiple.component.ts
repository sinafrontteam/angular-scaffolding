import {
  AfterViewInit,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {SelectItem} from 'primeng/api';
import {MultiSelect} from 'primeng/primeng';

import {keyboardKeys} from '../../constants/keyboard-keys.constants';
import {IdName} from '../../models/id-name.model';
import {ValidationElementComponent} from '../validation-element/validation-element.component';

const DEFAULT_SIZE = '50';

@Component({
  selector: 'si-selector-multiple',
  templateUrl: './selector-multiple.component.html',
  styleUrls: ['./selector-multiple.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SelectorMultipleComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() id: string;
  @Input() name: string;
  @Input() text: string;
  @Input() class: string;
  @Input() disabled: boolean;
  @Input() rule: string;
  @Input() labelAttr = 'name';
  @Input() valueAttr: string;
  @Input() tooltipText: string;
  @Input() tooltipPosition: string;
  @Input() tooltipIcon: string;
  @Input() detailMode = false;
  @Input() backSearchParam = 'q';
  @Output() modelChange = new EventEmitter<any>();
  @Output() selectDefaultChange = new EventEmitter<any>();
  @ViewChild('pMultiSelect') pMultiSelect: MultiSelect;
  @ViewChild('siSelectorMultipleLoader') siSelectorMultipleLoader: ElementRef;
  @ViewChild('validationElement') validationElement: ValidationElementComponent;
  @ContentChild('selectedItems') selectedItems: TemplateRef<ElementRef>;
  @ContentChild('item') item: TemplateRef<ElementRef>;
  options: SelectItem[];
  placeholderTranslated: string;
  inputFilterSubject = new Subject<string>();
  shiftTabFromInput = false;
  scrollHeight = '200px';
  displaySelectedLabel = true;
  filter = true;
  resetFilterOnHide = true;
  private clearIconElement: HTMLElement;
  private _listeners = [];
  private preselectedOptionIndex = -1;
  private highlightClass = 'si-selector-multiple_item--highlight';
  private _model: IdName[];
  private _placeholder: string;
  private _values: object[];
  private _path: string;
  private _backSearch = false;
  private _selectDefault = false;
  private _compact = false;

  constructor(private translateService: TranslateService, private http: HttpClient, private renderer: Renderer2) {
  }

  get model() {
    return this._model;
  }

  @Input()
  set model(val: IdName[]) {
    if (!val || val.length === 0 || val[0] instanceof Object) {
      this._model = val;
    }

    setTimeout(() => {
      this.loadDefaultModel(val);
    });

    if (!val || val.length === 0) {
      this.displayClearIcon(false);
    } else {
      this.displayClearIcon(true);
    }
  }

  get placeholder() {
    return this._placeholder;
  }

  @Input()
  set placeholder(val) {
    this._placeholder = val;
    this.translateService.get(val).toPromise().then(translation => {
      this.placeholderTranslated = translation;
    });
  }

  get values() {
    return this._values;
  }

  @Input()
  set values(val) {
    this.setValuesAsync(val);
  }

  get path() {
    return this._path;
  }

  @Input()
  set path(val) {
    this._path = val;
    this.options = null;
    // If another field changes, the path changes. So, we need to check if the options selected are valid and update them if it is necessary.
    this.setModelValid();
  }

  get backSearch() {
    return this._backSearch;
  }

  @Input()
  set backSearch(val) {
    // Backsearch is true when val is true or when val is ''.
    if (val === false) {
      this._backSearch = false;
    } else {
      this._backSearch = true;
    }
  }

  get compact() {
    return this._compact;
  }

  @Input()
  set compact(val) {
    this._compact = val;
    if (val) {
      this.scrollHeight = '300px';
      this.displaySelectedLabel = false;
      this.filter = false;
      this.resetFilterOnHide = false;
    }
  }

  get selectDefault() {
    return this._selectDefault;
  }

  @Input()
  set selectDefault(val) {
    this._selectDefault = true;
    this.getOptions();
  }

  async setValuesAsync(val): Promise<void> {
    const valuesTranslated = await this.translateValues(val);
    this._values = valuesTranslated;
    this.options = this.mapOptionsFromBack(valuesTranslated);
  }

  ngOnInit() {
    this.decorateMultiSelect();

    this.inputFilterSubject.asObservable()
      .pipe(debounceTime(400), distinctUntilChanged())
      .subscribe(() => {
        this.getOptions();
      });
  }

  ngAfterViewInit() {
    if (!this.compact && !this.detailMode) {
      this.setClearIcon();
    }

    // If the model comes with value, it is set before ngAfterViewInit, so we have to check it to display the clear icon after creating it.
    if (this.model && this.model.length) {
      this.displayClearIcon();
    }
  }


  setClearIcon(): void {
    this.clearIconElement = this.renderer.createElement('i');
    this.renderer.addClass(this.clearIconElement, 'icon-closex');
    this.renderer.addClass(this.clearIconElement, 'si-selector-multiple_clean_icon');
    this.renderer.addClass(this.clearIconElement, 'display-none');

    const pMultiSelectElement = this.pMultiSelect.el.nativeElement;
    const uiMultiSelectElement = pMultiSelectElement.querySelector('.ui-multiselect');
    const uiTriggerIconElement = pMultiSelectElement.querySelector('.ui-multiselect-trigger');
    this.renderer.insertBefore(uiMultiSelectElement, this.clearIconElement, uiTriggerIconElement);
    this._listeners.push(
      this.renderer.listen(this.clearIconElement, 'click', (event) => this.onClickClearIcon(event))
    );
  }

  displayClearIcon(displayFlag = true): void {
    if (!this.clearIconElement) {
      return;
    }

    if (displayFlag) {
      this.renderer.removeClass(this.clearIconElement, 'display-none');
    } else {
      this.renderer.addClass(this.clearIconElement, 'display-none');
    }
  }

  removeTabIndexOfOptions(): void {
    const pMultiSelectElement = this.pMultiSelect.el.nativeElement;
    const uiMultiSelectOptionElements = pMultiSelectElement.querySelectorAll('.ui-multiselect-item');
    uiMultiSelectOptionElements.forEach(el => this.renderer.removeAttribute(el, 'tabindex'));
  }

  onChangeMultiSelectModel({originalEvent, value}) {
    this.emitModel(value);
  }

  async onClickMultiSelect(): Promise<void> {
    if (this.pMultiSelect.overlayVisible) {
      return;
    }

    this.preselectedOptionIndex = -1;
    await this.getOptions();
    setTimeout(() => {
      this.removeTabIndexOfOptions();
    });
  }

  onHideMultiSelect(): void {
    if (this.backSearch) {
      this.options = [];
    }
  }

  onClickClearIcon(event): void {
    event.stopPropagation();
    this.removeOptions();
  }

  async getOptions(): Promise<void> {
    if (this.options && !this.backSearch) {
      return;
    }

    let httpParams = new HttpParams();
    if (this.backSearch) {
      httpParams = httpParams
        .set('size', DEFAULT_SIZE)
        .set(this.backSearchParam, this.pMultiSelect.filterValue || '');
    }

    this.renderer.addClass(this.siSelectorMultipleLoader.nativeElement, 'loading');
    const options = await this.http.get(this.path, {params: httpParams}).toPromise();
    this.options = this.mapOptionsFromBack(options);
    this.pMultiSelect.visibleOptions = this.options;
    this.renderer.removeClass(this.siSelectorMultipleLoader.nativeElement, 'loading');
    // Align the overlay element again after loading the options.
    this.pMultiSelect.alignOverlay();
  }

  mapOptionsFromBack(options): SelectItem[] {
    if (!options || !options.length) {
      return [];
    }

    let defaultOptionFlag = false;
    const optionsParsed = options.map(option => {
      const optionParsed = {
        label: option[this.labelAttr],
        value: option
      };

      if (this.selectDefault && option.isDefault) {
        if (!this.model) {
          this.model = [];
        }

        // Do not add the option if the model already includes that option.
        if (!this.model.find(modelOption => modelOption[this.valueAttr || 'id'] === option[this.valueAttr || 'id'])) {
          defaultOptionFlag = true;
          this.model.push(option);
        }
      }

      return optionParsed;
    });

    if (defaultOptionFlag) {
      this.emitModel(this.model);
      this.selectDefaultChange.emit();
    }

    return optionsParsed;
  }

  async translateValues(values): Promise<object[]> {
    for (let i = 0; i < values.length; i++) {
      values[i][this.labelAttr] = await this.translateService.get(values[i][this.labelAttr]).toPromise();
    }

    return values;
  }

  emitModel(model: IdName[]): void {
    if (this.valueAttr && model) {
      this.modelChange.emit(model.map(val => val[this.valueAttr]));
    } else {
      this.modelChange.emit(model);
    }
  }

  // Load a default model passed by parameter. It works like the old af-select-it.
  async loadDefaultModel(val: IdName[]): Promise<void> {
    if (val && val.length) {
      // If val is an object, set it as normal.
      // Else, it has to be an id. So, we have to retrieve the options, find the object by the id and set it in selectedOption.
      if (val[0] instanceof Object) {
        this.pMultiSelect.value = val;
        this.pMultiSelect.updateLabel();
      } else {
        await this.getOptions();
        this.pMultiSelect.value = this.options
          .filter(option => val.includes(option.value[this.valueAttr || 'id']))
          .map(option => option.value);
        this._model = this.pMultiSelect.value;
        // Check again to display clear icon due to:
        // 1) When the component is initialized the clear icon is not created yet.
        // 2) When ngAfterViewInit is launched, the model is not filled yet because we are waiting to get the options.
        if (!this._model || this._model.length === 0) {
          this.displayClearIcon(false);
        } else {
          this.displayClearIcon(true);
        }
      }
    }
  }

  // Check if each option of the model is in the options and set only the valid ones.
  async setModelValid(): Promise<void> {
    if ((!this.model || !this.model.length) && !this.validationElement.showRequiredElementFlag) {
      return;
    }

    if (!this.options || !this.options.length) {
      await this.getOptions();
    }

    if (this.model && this.model.length) {
      this.model = this.model.filter(modelOption => {
        return this.options.some(option => option.value[this.valueAttr || 'id'] === modelOption[this.valueAttr || 'id']);
      });

      this.emitModel(this.model);
    } else if (this.validationElement.showRequiredElementFlag && this.options && this.options.length === 1) {
      // Set the model if the selector is required and there is only one option to select.
      if (!this.model) {
        this.model = [];
      }

      this.model.push(this.options[0].value);
      this.emitModel(this.model);
    }
  }

  openMultiSelect(): void {
    this.pMultiSelect.selfClick = true;
    this.onClickMultiSelect();
    this.pMultiSelect.show();
  }

  removeOption(event, item): void {
    event.stopPropagation();

    const selectionIndex = this.pMultiSelect.findSelectionIndex(item);
    this.pMultiSelect.value = this.pMultiSelect.value.filter(function (val, i) { return i != selectionIndex; });
    if (this.pMultiSelect.selectionLimit) {
      this.pMultiSelect.maxSelectionLimitReached = false;
    }

    this.pMultiSelect.onModelChange(this.pMultiSelect.value);
    this.pMultiSelect.onChange.emit({ originalEvent: event.originalEvent, value: this.pMultiSelect.value, itemValue: item });
    this.pMultiSelect.updateLabel();
    this.pMultiSelect.updateFilledState();
  }

  removeOptions(): void {
    this.displayClearIcon(false);
    this.pMultiSelect.value = [];
    if (this.pMultiSelect.overlayVisible) {
      this.pMultiSelect.hide();
    }

    this.modelChange.emit([]);
  }

  // Functions overwriting the real ones in the MultiSelect component.
  async decorateMultiSelect(): Promise<void> {
    if (this.pMultiSelect) {
      // ActivateFilter overwritten because the pMultiSelect only filter in front.
      // So, we implement the inputFilterSubject to filter in back.
      const originalActivateFilter = this.pMultiSelect.activateFilter.bind(this.pMultiSelect);
      this.pMultiSelect.activateFilter = () => {
        if (!this.backSearch) {
          originalActivateFilter();
          return;
        }

        this.inputFilterSubject.next(this.pMultiSelect.filterValue);
      };

      // OnFilter overwritten because when the filter is in back we need to filter even when the inputValue is null.
      this.pMultiSelect.onFilter = () => {
        this.resetHighlightOptions();
        const inputValue = this.pMultiSelect.filterInputChild.nativeElement.value;
        if (this.backSearch || inputValue && inputValue.length) {
          this.pMultiSelect.filterValue = inputValue;
          this.pMultiSelect.activateFilter();
        } else {
          this.pMultiSelect.filterValue = null;
          this.pMultiSelect.visibleOptions = this.options;
          this.pMultiSelect.filtered = false;
        }
      };

      // UpdateLabel overwritten because we want the label even if an option is not in the options.
      this.pMultiSelect.updateLabel = () => {
        if (this.pMultiSelect.value && this.pMultiSelect.value.length && this.pMultiSelect.displaySelectedLabel) {
          let label = '';
          // Add option labels only until maxSelectedLabels is reached.
          for (let i = 0; i < this.pMultiSelect.value.length && i < this.pMultiSelect.maxSelectedLabels; i++) {
            // If the option is not in options (or options does not exist), get its name: this.value[i].name
            let optionLabel = this.pMultiSelect.options ? this.pMultiSelect.findLabelByValue(this.pMultiSelect.value[i]) : null;
            optionLabel = optionLabel ? optionLabel : this.pMultiSelect.value[i][this.labelAttr];
            label = label.length > 0 ? label + ', ' : label;
            label = label + optionLabel;
          }

          if (label.length > 50) {
            label = `${label.substring(0, 49)} ...`;
          }

          this.pMultiSelect.valuesAsString = label;
        } else {
          this.pMultiSelect.valuesAsString = this.pMultiSelect.defaultLabel;
        }
      };

      // OnOptionClick overwritten because we want to keep the focus on the filter input and allow the user to select options with keys.
      const originalOnOptionClick = this.pMultiSelect.onOptionClick.bind(this.pMultiSelect);
      this.pMultiSelect.onOptionClick = (event) => {
        const optionElements = this.pMultiSelect.overlay.querySelector('.ui-multiselect-items').children;
        // Remove highlight class of previous preselected option.
        if (this.preselectedOptionIndex >= 0) {
          this.renderer.removeClass(optionElements[this.preselectedOptionIndex].firstChild, this.highlightClass);
        }

        // Assign the new preselected option index.
        this.preselectedOptionIndex = this.options.findIndex(option => option.value.id === event.option.value.id);
        this.renderer.addClass(optionElements[this.preselectedOptionIndex].firstChild, this.highlightClass);
        // Focus on input filter.
        if (this.pMultiSelect.filterInputChild) {
          this.pMultiSelect.filterInputChild.nativeElement.focus();
        }

        originalOnOptionClick(event);
      };

      // Show overwritten because we want to allow the user to select options with keys.
      const originalShow = this.pMultiSelect.show.bind(this.pMultiSelect);
      this.pMultiSelect.show = () => {
        originalShow(event);

        setTimeout(() => {
          if (this.pMultiSelect.filterInputChild) {
            this._listeners.push(
              this.renderer.listen(this.pMultiSelect.filterInputChild.nativeElement, 'keydown', () => this.onKeyDownInput(event))
            );
          }
        });
      };

      // AlignOverlay overwritten to add 2 pixels to the overlay element because if not, the blue animation cannot be seen.
      const originalAlignOverlay = this.pMultiSelect.alignOverlay.bind(this.pMultiSelect);
      this.pMultiSelect.alignOverlay = function () {
        setTimeout(() => {
          originalAlignOverlay();
          if (this.overlay) {
            const top = this.overlay.style.top;
            this.overlay.style.top = (parseInt(top.substring(0, top.length - 2), 10) - 2).toString() + 'px';
          }
        });
      };
    }
  }

  highlightPrevOption(): void {
    if (!this.options || !this.options.length || this.preselectedOptionIndex === 0) {
      return;
    }

    this.highlightOption(this.preselectedOptionIndex - 1, this.preselectedOptionIndex);
    this.preselectedOptionIndex--;
  }

  highlightNextOption(): void {
    if (!this.options || !this.options.length || this.preselectedOptionIndex === this.options.length - 1) {
      return;
    }

    this.highlightOption(this.preselectedOptionIndex + 1, this.preselectedOptionIndex);
    this.preselectedOptionIndex++;
  }

  highlightOption(index: number, previousIndex = -1): void {
    const optionElements = this.pMultiSelect.overlay.querySelector('.ui-multiselect-items').children;
    if (previousIndex !== -1) {
      this.renderer.removeClass(optionElements[previousIndex].firstChild, this.highlightClass);
    }

    const elementToHighlight = optionElements[index].firstChild;
    this.renderer.addClass(elementToHighlight, this.highlightClass);

    this.scrollToOption(elementToHighlight);
  }

  resetHighlightOptions(): void {
    // Remove highlight class of previous preselected option.
    if (this.preselectedOptionIndex >= 0) {
      const optionElements = this.pMultiSelect.overlay.querySelector('.ui-multiselect-items').children;
      if (optionElements[this.preselectedOptionIndex]) {
        this.renderer.removeClass(optionElements[this.preselectedOptionIndex].firstChild, this.highlightClass);
      }
    }

    this.preselectedOptionIndex = -1;
  }

  scrollToOption(element): void {
    const wrapperElement = this.pMultiSelect.overlay.querySelector('.ui-multiselect-items-wrapper');

    // Element outside wrapper above (scrolling up).
    const positionTopElement = element['offsetTop'] - wrapperElement.scrollTop;
    if (positionTopElement < 0) {
      wrapperElement.scroll({top: element['offsetTop']});
    }

    // Element outside wrapper below (scrolling down).
    const positionBottomElement = element['offsetTop'] - wrapperElement.scrollTop + element['offsetHeight'];
    const wrapperHeight = wrapperElement['offsetHeight'];
    if (positionBottomElement > wrapperHeight) {
      wrapperElement.scroll({top: element['offsetTop'] + element['offsetHeight'] - wrapperHeight});
      return;
    }
  }

  // Events on the input filter.
  onKeyDownInput(event): void {
    switch (event.key) {
      case keyboardKeys.ARROW_DOWN:
        this.highlightNextOption();
        event.preventDefault();
        break;
      case keyboardKeys.ARROW_UP:
        this.highlightPrevOption();
        event.preventDefault();
        break;
      case keyboardKeys.ENTER:
        const e = {
          originalEvent: event,
          option: this.pMultiSelect.options[this.preselectedOptionIndex]
        };
        this.pMultiSelect.onOptionClick(e);
        break;
      case keyboardKeys.TAB:
        if (event.shiftKey) {
          this.shiftTabFromInput = true;
        }

        this.pMultiSelect.hide();
        break;
      default:
    }
  }

  @HostListener('keyup', ['$event'])
  async keyEvent(event: KeyboardEvent) {
    if (event.key === keyboardKeys.TAB) {
      if (event.shiftKey && this.shiftTabFromInput) {
        this.shiftTabFromInput = false;
        return;
      }

      if (!this.pMultiSelect.overlayVisible) {
        await this.onClickMultiSelect();
        this.pMultiSelect.show();
      }
    }
  }

  ngOnDestroy() {
    this.inputFilterSubject.complete();
    this._listeners.forEach(fn => fn());
  }

}
