import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {localeKeyShort} from '../../constants/i18n.constants';

@Component({
  selector: 'rvb-translation',
  templateUrl: './translation.component.html',
  styleUrls: ['./translation.component.scss']
})
export class TranslationComponent implements OnInit {
  localeKeyShort = localeKeyShort;
  activeLang: string;

  constructor(private translate: TranslateService) {
  }

  ngOnInit() {
  }

  changeLanguage(lang: string): void {
    this.activeLang = lang;
    this.translate.use(lang);
    //TODO en tiempo de ejecución no se está cambiando el idioma interno de los datepicker
  }
}
