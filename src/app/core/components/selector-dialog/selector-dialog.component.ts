import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IdName} from '../../models/id-name.model';

@Component({
  selector: 'si-selector-dialog',
  templateUrl: './selector-dialog.component.html',
  styleUrls: ['./selector-dialog.component.scss']
})
export class SelectorDialogComponent {
  @Input() model: any; // IdName | string

  // Type of the model
  @Input() type: string;
  @Input() text: string;
  @Input() placeholder: string;
  @Input() disabled: boolean;
  @Input() rule: string;
  @Input() tooltipText: string;
  @Input() tooltipPosition: string;
  @Input() tooltipIcon: string;

  // Needed if the component is being used in AngularJS view and it needs validation.
  @Input() ng1Form: any;
  @Input() showClear = true;
  @Input() displayDialog = false;
  @Output() displayDialogChange = new EventEmitter<boolean>();
  @Output() modelChange = new EventEmitter<IdName | string>();

  constructor() {
  }

  onClickClearIcon(event): void {
    event.stopPropagation();
    this.model = null;
    this.modelChange.emit(this.model);
  }

  onClickShowDialog(): void {
    this.displayDialog = true;
    this.displayDialogChange.emit(this.displayDialog);
  }
}
