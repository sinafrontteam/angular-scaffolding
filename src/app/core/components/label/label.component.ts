import {Component, Input} from '@angular/core';

@Component({
  selector: 'si-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.scss']
})
export class LabelComponent {
  @Input() text: string;
  @Input() tooltipText: string;
  private _tooltipPosition = 'top';
  private _tooltipIcon = 'icon-infocircle';

  get tooltipPosition() {
    return this._tooltipPosition;
  }

  @Input()
  set tooltipPosition(val) {
    if (val) {
      this._tooltipPosition = val;
    }
  }

  get tooltipIcon() {
    return this._tooltipIcon;
  }

  @Input()
  set tooltipIcon(val) {
    if (val) {
      this._tooltipIcon = val;
    }
  }
}
