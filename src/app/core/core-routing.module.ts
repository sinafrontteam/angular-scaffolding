import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HolaPageComponent} from './documentation/hola-page/hola.component';

const routes: Routes = [
  {
    path: 'core',
    children: [
      {
        path: '',
        redirectTo: 'doc',
        pathMatch: 'full'
      },
      {
        path: 'doc',
        children: [
          {
            path: '',
            redirectTo: 'hola',
            pathMatch: 'full'
          },
          {
            path: 'hola',
            component: HolaPageComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule {
}
