import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MomentModule} from 'ngx-moment';
import 'moment/locale/es';
import 'moment/locale/en-gb';
import 'moment/locale/fr';

import {ValidationDirective} from './directives/validation.directive';
import {ValidationElementComponent} from './components/validation-element/validation-element.component';

import {TranslateModule} from '@ngx-translate/core';
import {HttpClientModule} from '@angular/common/http';
import {TranslationComponent} from './components/translation/translation.component';

import {DialogModule} from 'primeng/dialog';
import {TooltipModule} from 'primeng/tooltip';
import {DropdownModule} from 'primeng/dropdown';
import {MultiSelectModule} from 'primeng/multiselect';
import {TabViewModule} from 'primeng/tabview';
import {TableModule} from 'primeng/table';
import {CalendarModule} from 'primeng/calendar';
import {AccordionModule} from 'primeng/accordion';
import {EditorModule} from 'primeng/editor';
import {PaginatorModule} from 'primeng/paginator';
import {SidebarModule} from 'primeng/sidebar';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {PanelMenuModule} from 'primeng/panelmenu';
import {CoreRoutingModule} from './core-routing.module';
import {InputComponent} from './components/input/input.component';
import {LabelComponent} from './components/label/label.component';
import {CheckComponent} from './components/check/check.component';
import {DatepickerComponent} from './components/datepicker/datepicker.component';
import {MultilineComponent} from './components/multiline/multiline.component';
import {OverlayPanelComponent} from './components/overlay-panel/overlay-panel.component';
import {RadioComponent} from './components/radio/radio.component';
import {RadioDynamicComponent} from './components/radio-dynamic/radio-dynamic.component';
import {RichTextComponent} from './components/rich-text/rich-text.component';
import {SelectorComponent} from './components/selector/selector.component';
import {SelectorDialogComponent} from './components/selector-dialog/selector-dialog.component';
import {SelectorMultipleComponent} from './components/selector-multiple/selector-multiple.component';
import {TableComponent} from './components/table/table.component';
import {ListDynamicComponent} from './components/list-dynamic/list-dynamic.component';
import {LoaderComponent} from '@modules/core/components/loader/loader.component';
import {HolaPageComponent} from './documentation/hola-page/hola.component';
import {ProvideParentFormDirective} from '@modules/core/directives/provide-parent-form.directive';

@NgModule({
  declarations: [
    TranslationComponent,
    ValidationDirective,
    ValidationElementComponent,
    ProvideParentFormDirective,
    InputComponent,
    LabelComponent,
    CheckComponent,
    DatepickerComponent,
    MultilineComponent,
    OverlayPanelComponent,
    RadioComponent,
    RadioDynamicComponent,
    RichTextComponent,
    SelectorComponent,
    SelectorDialogComponent,
    SelectorMultipleComponent,
    TableComponent,
    ListDynamicComponent,
    LoaderComponent,
    HolaPageComponent
  ],
  imports: [
    CommonModule,
    CoreRoutingModule,
    FormsModule,
    HttpClientModule,
    MomentModule,
    TranslateModule,
    DialogModule,
    TooltipModule,
    DropdownModule,
    MultiSelectModule,
    TabViewModule,
    TableModule,
    CalendarModule,
    AccordionModule,
    EditorModule,
    PaginatorModule,
    SidebarModule,
    OverlayPanelModule
  ],
  exports: [
    MomentModule,
    DialogModule,
    TooltipModule,
    DropdownModule,
    MultiSelectModule,
    TabViewModule,
    TableModule,
    AccordionModule,
    EditorModule,
    PaginatorModule,
    SidebarModule,
    OverlayPanelModule,
    TranslationComponent,
    InputComponent,
    LabelComponent,
    CheckComponent,
    DatepickerComponent,
    MultilineComponent,
    OverlayPanelComponent,
    PanelMenuModule,
    RadioComponent,
    RadioDynamicComponent,
    RichTextComponent,
    SelectorComponent,
    SelectorDialogComponent,
    SelectorMultipleComponent,
    TableComponent,
    ListDynamicComponent,
    LoaderComponent
  ]
})
export class CoreModule {
}
