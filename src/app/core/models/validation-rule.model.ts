export class ValidationRule {
  message: string;
  arguments?: any[];
}
