export class IdName {
  id: number;
  name?: string;
  code?: string;
  isDefault?: boolean;
}
