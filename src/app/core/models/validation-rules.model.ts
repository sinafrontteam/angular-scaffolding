import {ValidationRule} from './validation-rule.model';

export class ValidationRules {
  [key: string]: ValidationRule
}
