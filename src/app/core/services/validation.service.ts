import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {
  rules: any;

  constructor() {
  }

  getRules(): any {
    return this.rules;
  }

  setRules(rules: any): void {
    this.rules = rules;
  }
}
