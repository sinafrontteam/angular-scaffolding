import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HolaPageComponent } from './hola-page.component';

describe('HolaPageComponent', () => {
  let component: HolaPageComponent;
  let fixture: ComponentFixture<HolaPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HolaPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HolaPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
