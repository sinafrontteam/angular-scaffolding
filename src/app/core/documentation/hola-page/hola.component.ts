import { Component, OnInit } from '@angular/core';
import {IdName} from '@modules/core/models/id-name.model';

@Component({
  selector: 'si-hola-page',
  templateUrl: './hola-page.component.html',
  styleUrls: ['./hola-page.component.scss']
})
export class HolaPageComponent implements OnInit {
  semanas: string;
  semanasNum: number;
  saveLocSession: boolean;
  startDate: number;
  observationsAdministrative: string;
  disableFieldsFlag = false;
  radioDynamicVariable: IdName;
  usualMedication: string;
  localization: IdName;
  localizationMultiple: IdName[];
  initialState: IdName;
  displayScalesDialog = false;
  selectedPatient;
  pathologicAnatomy = false;
  patients = [
    {
      name: 'Sergio',
      surname: 'Fernández',
      birthDate: '11/08/1645',
      dni: '45264815M',
      patientId: '753646',
      id: 0
    },
    {
      name: 'Marta',
      surname: 'Palau',
      birthDate: '02/08/2012',
      dni: '49466512H',
      patientId: '753799',
      id: 1
    },
    {
      name: 'Antonio',
      surname: 'Mota',
      birthDate: '07/08/1991',
      dni: '45264815M',
      patientId: '753162',
      id: 2
    },
    {
      name: 'Eloy',
      surname: 'Galdón',
      birthDate: '09/08/2000',
      dni: '49466512H',
      id: 3
    },
    {
      name: 'Sergio',
      surname: 'Fernández',
      birthDate: '11/08/1645',
      dni: '45264815M',
      id: 4
    },
    {
      name: 'Marta',
      surname: 'Palau',
      birthDate: '02/08/2012',
      dni: '49466512H',
      id: 5
    },
    {
      name: 'Antonio',
      surname: 'Mota',
      birthDate: '07/08/1991',
      dni: '45264815M',
      id: 6
    }
  ];

  constructor() { }

  ngOnInit() {
  }

  onRowSelect(row): void {

  }

  onColResize(row): void {

  }

}
