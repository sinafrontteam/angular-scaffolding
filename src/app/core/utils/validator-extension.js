﻿
(function(validator) {
    var extensions = {
        length: function (str, param1, param2) {
            return (str === undefined || str === null || str === '') || (str.length >= param1 && str.length <= param2);
        },
        required: function (str) {
            return str !== undefined && str !== '';
        },
        true: function (str) {
            return str == 'true';
        },
        past: function (str) {
            if (str) {
                var time = null;
                if (parseInt(str) == str) {
                    time = moment(parseInt(str));
                } else {
                    time = moment(str);
                }
                return time ? time.isBefore(moment(), 'day') : true;
            } else {
                return true;
            }
        },
        pastToday: function (str) {
            if (str) {
                var time = null;
                if (parseInt(str) == str) {
                    time = moment(parseInt(str));
                } else {
                    time = moment(str);
                }
                return time ? time.isSameOrBefore(moment(), 'day') : true;
            } else {
                return true;
            }
        },
        future: function (str, param1) {
            if(str) {
                var time = null;
                if (parseInt(str) == str) {
                    time = moment(parseInt(str));
                } else {
                    time = moment(str);
                }
                return moment().subtract(10, 'm').isBefore(time);
            } else {
                return true;
            }
        },
        futureToday: function (str, param1) {
            if(str) {
                var time = null;
                if (parseInt(str) == str) {
                    time = moment(parseInt(str));
                } else {
                    time = moment(str);
                }
                return time.valueOf() >=  moment().startOf('day').valueOf();
            } else {
                return true;
            }
        },
        notNull: function (str) {
            return str !== undefined && str !== '';
        },
        notBlank: function (str) {
            return !(str && (str.trim() === '')); //this is only back verification (str !== undefined) && (str === "");
        },
        alphabetical: function (str) {
            return (str === null) || (str === "") || (str.match(/\d+/) === null);
        },
        pattern: function (str, patt) {
            var re = new RegExp(patt);
            return (str == null) || (str == "") || re.test(str);
        },
        checkDatesOutbound: function(str, scopeValue){
            if(str && scopeValue) {
                var returnDate = scopeValue;
                var outboundDate = moment(new Date(str));
                return moment(outboundDate).isSameOrBefore(returnDate,'day');
            } else {
                return true;
            }
        },
        checkDatesReturn: function(str, scopeValue){
            if(str && scopeValue) {
                var returnDate = moment(new Date(str));
                var outboundDate = scopeValue;
                return moment(returnDate).isSameOrAfter(outboundDate,'day');
            } else {
                return true;
            }
        },
        contactStartTime: function(str, scopeValue){
            if(!str, !scopeValue){
                return true;
            }else if(!str){
                return false;
            }
            var startHour = scopeValue - moment(moment(scopeValue).format('DD/MM/YYYY'),'DD/MM/YYYY').valueOf();
            var endHour = moment(new Date(str)) - moment(moment(new Date(str)).format('DD/MM/YYYY'),'DD/MM/YYYY').valueOf();
            return startHour < endHour ? false : true;
        },
        contactEndTime: function(str, scopeValue){
            if(!str, !scopeValue){
                return true;
            }else if(!str){
                return false;
            }
            var startHour = scopeValue - moment(moment(scopeValue).format('DD/MM/YYYY'),'DD/MM/YYYY').valueOf();
            var endHour = moment(new Date(str)) - moment(moment(new Date(str)).format('DD/MM/YYYY'),'DD/MM/YYYY').valueOf();
            return startHour > endHour ? false : true;
        },
        contactStartTimeNotIntroduced: function(str, scopeValue){
            if(!str && scopeValue){
                return false;
            }else{
                return true;
            }
        },
        contactEndTimeNotIntroduced: function(str, scopeValue) {
            if (!str && scopeValue) {
                return false;
            }
            else {
                return true;
            }
        },
        alphanumeric: function(str) {
            if (str) {
                var regex = /^[0-9A-Z]+$/i;
                return regex.test(str);
            }
            else {
                return true;
            }
        },
        customCode:function(value, fn, scope) {
            if (!scope.$parent.formModel) {
                scope.$parent.formModel = {};
                scope.$parent.formModel.model = {};
            }
            var F = new Function ('value', fn);
            return F.bind(scope.$parent.formModel.model)(value);
        },
        minmax: function (str, param1, param2) {
            str=parseInt(str);
            return isNaN(str) || str >= param1 && str <= param2;
        },
        firstCharacterIsLetter: function(str) {
            return (str === null) || (str === "") || (str[0].match(/\d+/) === null);
        },
        specialCharacter: function (str, patt) {
            var re = new RegExp(patt);
            return (str == null) || (str == "") || !re.test(str);
        }
    };

    Object.keys(extensions).forEach(function (key) {
        validator.extend(key, extensions[key]);
    });
}(validator));
