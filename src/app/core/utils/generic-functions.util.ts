export class GenericFunctionsUtil {

  /**
   * Find value in nested object by nested key.
   *
   * Example:
   * const myObject = {
   *   keyA: {
   *     keyAB: 2
   *   }
   * };
   *
   * findInObjectByNestedKey('keyA.keyB', myObject); // Returns 2
   *
   */
  public static findInObjectByNestedKey(path: string | string[], obj: any = self, separator: string = '.'): any {
    const properties = Array.isArray(path) ? path : path.split(separator);
    return properties.reduce((prev, curr) => prev && prev[curr], obj);
  }

  /**
   * Replace arguments in a string.
   *
   * Example:
   * let myText = 'Hello {0} and {1}';
   * myText = stringReplaceArguments(myText, ['Front', 'Back']); // Returns 'Hello Front and Back'
   *
   */
  public static stringReplaceArguments(text: string, params: any[] = []): string {
    for (let i = 0; i < params.length; i++) {
      const regEx = new RegExp('\\{' + (i) + '\\}', 'gm');
      text = text.replace(regEx, params[i].toString());
    }

    return text;
  };
}
