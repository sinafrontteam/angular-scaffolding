import {AfterViewInit, Directive, Input, OnDestroy} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, Validator} from '@angular/forms';
import {GenericFunctionsUtil} from '../utils/generic-functions.util';
import {ValidationElementComponent} from '../components/validation-element/validation-element.component';
import {TranslateService} from '@ngx-translate/core';
import {ValidationRules} from '../models/validation-rules.model';
import {DispatcherService} from '../services/dispatcher.service';
import {ValidationService} from '@modules/core/services/validation.service';

@Directive({
  selector: '[siValidation]',
  providers: [{provide: NG_VALIDATORS, useExisting: ValidationDirective, multi: true}]
})
export class ValidationDirective implements Validator, AfterViewInit, OnDestroy {
  @Input() siValidation: string;
  @Input() siValidationElement: ValidationElementComponent;
  validator = window['validator'];
  sinaRules: object;
  validationRules: ValidationRules;
  validationRulesKeys: string[];
  observerKey = `validation${Math.random().toString(36).substr(2, 6)}`;
  formControl: AbstractControl;

  constructor(private translateService: TranslateService,
              private dispatcher: DispatcherService,
              private validationService: ValidationService) {
    this.sinaRules = this.validationService.getRules();
  }

  ngAfterViewInit() {
    if (!this.siValidation) {
      return;
    }

    this.init();
    this.dispatcher.register('validation-rules-updated', () => this.onValidationRulesUpdated(), this.observerKey);
  }

  init(): void {
    this.validationRules = GenericFunctionsUtil.findInObjectByNestedKey(this.siValidation, this.sinaRules) || {};
    this.validationRulesKeys = Object.keys(this.validationRules);
    this.translateValidationMessages();
    this.manageRequiredWarning();
  }

  onValidationRulesUpdated(): void {
    this.init();
    this.formControl.updateValueAndValidity();
  }

  async translateValidationMessages(): Promise<void> {
    for (let i = 0; i < this.validationRulesKeys.length; i++) {
      const key = this.validationRulesKeys[i];
      this.validationRules[key].message = await this.translateService.get(this.validationRules[key].message).toPromise();
    }
  }

  manageRequiredWarning(): void {
    if (this.siValidationElement && this.validationRulesKeys.indexOf('notNull') > -1) {
      this.siValidationElement.showRequiredElement();
    }
  }

  validate(control: AbstractControl): { [key: string]: any } | null {
    this.formControl = control;
    if (!this.validationRules) {
      return;
    }

    const invalidKey = this.validationRulesKeys.find(key => {
      const validatorParams = [control.value].concat(this.validationRules[key].arguments || []);
      return !this.validator[key].apply(this.validator, validatorParams);
    });

    if (invalidKey) {
      this.siValidationElement.showWarning(invalidKey, this.validationRules[invalidKey]);
      return {
        [invalidKey]: {value: control.value}
      };
    } else {
      this.siValidationElement.hideWarning();
      return null;
    }
  }

  ngOnDestroy() {
    if (this.siValidation) {
      this.dispatcher.unregisterFunction('validation-rules-updated', this.observerKey);
    }
  }

}
